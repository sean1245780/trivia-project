#pragma once

// Constants Defines
#define RSSC

#include <unordered_map>
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <atomic>
#include "WSAInitializer.h"
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "SingletonTemplate.h"
#include "ostreamTS.h"
#include "json.hpp"

#define DEF_PORT 9512

using std::unordered_map;
using std::string;
using std::mutex;
using std::lock_guard;
using std::unique_lock;
using std::atomic;
using ostreamts::coutts;

class Communicator : public SingletonTemplate<Communicator>
{
private:
	// Variables
	RequestHandlerFactory& _handlerFactory;
	unordered_map<SOCKET, IRequestHandler*> _clients;
	SOCKET _serverSocket;
	WSAInitializer _WSA;
	mutex _clientsMapMutex;
	int _port;
	bool _binding; // For binding purposes
	bool _handling; // For handling puposes
	atomic<unsigned int> _threadCount;

	// Functions & Methods
	void handleNewClient(const SOCKET clientSocket);
	void sendData(SOCKET socket, string data);
	char* getData(SOCKET socket, int bytesNum, int flags = 0);

	// Constructor
	Communicator(RequestHandlerFactory& handlerFactory, const int port = DEF_PORT);

	// Class Workout
	friend class SingletonTemplate;

public:
	// Variables
	atomic<bool> communication_work;

	// Constructors & Destructors
	virtual ~Communicator();

	// Functions & Methods
	void bindAndListen();
	void startHandleRequests();
};