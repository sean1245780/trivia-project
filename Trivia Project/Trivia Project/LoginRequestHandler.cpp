#include "LoginRequestHandler.h"

/*
	The function login the user if can.
	Input: request = The request to login the user by - const RequestInfo&;
	Output: The result of the login - RequestResult;
*/
RequestResult LoginRequestHandler::login(const RequestInfo& request)
{

	LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer);
	LoginResponse response{0};
	RequestResult result{"", nullptr};
	unsigned_byte status = 0;

	try
	{
		this->_handlerFactory.getLoginManager().login(loginRequest.username, loginRequest.password);

		response.status = SUCCESS;
		result.newHandler = this->_handlerFactory.createMenuRequestHandler(loginRequest.username);
		status = RSSC_LOGIN;
	}
	catch (const AnswerException& e)
	{
		response.status = FAIL;
		result.newHandler = nullptr;
		status = e.getStatus();
	}
	catch (const std::exception& e)
	{
		throw std::exception(string("Invalid signup process made! -> " + string(e.what())).c_str());
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	The function signning up the user if can.
	Input: request = The request to sign up the user by - const RequestInfo&;
	Output: The result of the sign up - RequestResult;
*/
RequestResult LoginRequestHandler::signup(const RequestInfo& request)
{
	
	SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer);
	SignupResponse response{0};
	RequestResult result{"", nullptr};
	unsigned_byte status = 0;

	try
	{
		this->_handlerFactory.getLoginManager().signup(signupRequest.username, signupRequest.password, signupRequest.email, signupRequest.address, signupRequest.phone_num, signupRequest.birth_date);

		response.status = SUCCESS;
		result.newHandler = this->_handlerFactory.createMenuRequestHandler(signupRequest.username);
		status = RSSC_SIGNUP;
	}
	catch(const AnswerException& e)
	{ 
		response.status = FAIL;
		result.newHandler = nullptr;
		status = e.getStatus();
	}
	catch (const std::exception& e)
	{
		throw std::exception(string("Invalid signup process made! -> " + string(e.what())).c_str());
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	The function initizalizes the LoginRequestHandler class.
	Input: handlerFactory = The handler factory to initialize to - const RequestHandlerFactory&;
	Output: None;
*/
LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& handlerFactory) : _handlerFactory(handlerFactory)
{
}

/*
	The function destroys the LoginRequestHandler class.
	Input: None;
	Output: None;
*/
LoginRequestHandler::~LoginRequestHandler()
{
}

/*
	The function checks if the given request by the user is relevant to it current state.
	Input: info = The object that holds the request info - RequestInfo;
	Output: If the request is relevant - bool;
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
	return info.id == RQSC_LOGIN || info.id == RQSC_SIGNUP;
}

/*
	The function handles the given request by the user.
	Input: info = The object that holds the request info - RequestInfo;
	Output: The results of the given request by the protocol - RequestResult;
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{

	if (info.id == RQSC_LOGIN)
	{
		return this->login(info);
	}
	else if (info.id == RQSC_SIGNUP)
	{
		return this->signup(info);
	}

	return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ RSSC_ERR_MSG_REQST_RELEVANT }, RSSC_ERROR), nullptr };
}

bool LoginRequestHandler::disconnectUser()
{
	return false;
}
