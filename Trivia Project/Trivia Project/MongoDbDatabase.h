#pragma once

// Constants Defines
#define DBUSE

#define NOMINMAX

#include <WinSock2.h>
#include <Windows.h>

#include <mutex>

#include <bsoncxx/json.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/collection.hpp>
#include <mongocxx/pool.hpp>
#include <vector>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include "IDatabase.h"
#include "SingletonTemplate.h"
#include "json.hpp"
#include "Constants.h"

using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using bsoncxx::type;

using namespace mongocxx;
using namespace nlohmann;
using std::mutex;
using std::lock_guard;
using std::unique_lock;
using std::vector;
using std::unordered_map;
using std::unordered_set;
using std::stringstream;
using std::thread;

typedef struct clientEntry;

class MongoDbDatabase : public SingletonTemplate<MongoDbDatabase, IDatabase>
{
private:
	// Variables
	instance _instance;
	uri _uriConnection;
	pool _clientsPool;
	//unordered_map<thread::id, clientEntry> _clients;
	mutex _queryMutex;
	//mutex _clientsMutex;
	
	// Constructors
	MongoDbDatabase();

	// Functions & Methods
	//client& _acquireClient(const string& db_name = "");
	//database& _acquireDatabase(const string& db_name);

	// Class Workout
	friend class SingletonTemplate;

public:
	// Variables
	static const typeDBClasses classType;

	// Constructors & Destructors
	virtual ~MongoDbDatabase();

	// Methods & Functions
	virtual bool doesUserExist(const string& username) override;
	virtual bool doesUserExist(const string& username, const string& password) override;
	virtual bool doesPasswordMatch(const string& password) override;
	virtual void addNewUser(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate) override;
	virtual unordered_set<string> getQuestions(const int amount);
	virtual double getPlayerAverageAnswerTime(const string& username);
	virtual int getNumOfCorrectAnswers(const string& username);
	virtual int getNumOfTotalAnswers(const string& username);
	virtual int getNumOfPlayerGames(const string& username);
	virtual json getTopUserStatistics(const int limit);
	virtual json getUserStatistics(const string& username);
	virtual typeDBClasses getClassType() const;
	//void addClient();
	//bool deleteClient();
	//pool& getClientsPool(); // For safety reason should be a refrence
};

//typedef struct clientEntry
//{
//private:
//	pool::entry* _poolEntry;
//	database* _database;
//
//public:
//	clientEntry();
//	clientEntry(clientEntry&& other) noexcept;
//	clientEntry(clientEntry& other) noexcept;
//	clientEntry(pool& clientPool);
//	clientEntry(pool& clientPool, const string& db_name);
//	clientEntry& operator=(clientEntry& other) noexcept;
//	clientEntry& operator=(clientEntry&& other) noexcept;
//
//	~clientEntry();
//
//	client& setClient(pool& clientPool);
//
//	client& getClient() const;
//
//	database& setDatabase(const string& db_name);
//
//	database& getDatabase();
//
//	bool hasDatabase() const noexcept;
//
//} clientEntry;