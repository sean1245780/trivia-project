#pragma once

// Constants Defines
#define JRPD

#include <string>
#include "json.hpp"
#include <bitset>
#include <algorithm>
#include <sstream>
#include <iostream>
#include "Constants.h"

using std::string;
using std::stringstream;
using std::atoi;
using json = nlohmann::json;

typedef struct
{
public:
	string username;
	string password;

} LoginRequest;

typedef struct
{
public:
	string username;
	string password;
	string email;
	string address;
	string phone_num;
	string birth_date;

} SignupRequest;

typedef struct
{
public:
	unsigned_byte roomId;

} GetPlayersInRoomRequest;

typedef struct
{
public:
	unsigned int roomId;

} JoinRoomRequest;

typedef struct
{
public:
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;

class JsonRequestPacketDeserializer
{
private:
	// Constructors
	JsonRequestPacketDeserializer();
	static string strToNums(string val);

public:

	// Methods & Functions
	static LoginRequest deserializeLoginRequest(const string& buffer);
	static SignupRequest deserializeSignupRequest(const string& buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const string& buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const string& buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(const string& buffer);
};

