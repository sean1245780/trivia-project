#include "RequestHandlerFactory.h"

/*
	The function initizalizes the RequestHandlerFactory class.
	Input: database = The database to initialize to - IDatabase&;
	Output: None;
*/
RequestHandlerFactory::RequestHandlerFactory(IDatabase& database) : _database(database), _loginManager(LoginManager::getInstance<IDatabase&>(_database)), _roomManager(RoomManager::getInstance()), _statisticsManager(StatisticsManager::getInstance<IDatabase&>(_database))
{
}

/*
	The function destroys the RequestHandlerFactory class.
	Input: None;
	Output: None;
*/
RequestHandlerFactory::~RequestHandlerFactory()
{
}

/*
	The function creates the createLoginRequestHandler object and returns it.
	Input: None;
	Output: The LoginRequestHandler object that was created - LoginRequestHandler;
*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(*this);
}

/*
	The function creates a menu request handler
	Input: The username of the logged user
	Output: The menu request handler
*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(const string& username)
{
	auto loggedUsers = this->getLoginManager().getLoggedUsers();
	for (auto it = loggedUsers.begin(); it != loggedUsers.end(); it++)
	{
		if (it->getUsername() == username)
			return new MenuRequestHandler(*it, *this);
	}

	return nullptr;
}

/*
	The function return the class' LoginManager object.
	Input: None;
	Output: The LoginManager object that was request - LoginManager&;
*/
LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->_loginManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	
	return this->_statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	
	return this->_roomManager;
}

/*
	The functions check if the given database is MongoDB database and initialize the thread's client inside of it.
	Input: None;
	Output: If the initialization was successful - bool;
*/
//bool RequestHandlerFactory::initializeClientDB()
//{
//	if (this->_database.getClassType() == MongoDbDatabase::classType)
//	{
//		MongoDbDatabase& mongo_db = dynamic_cast<MongoDbDatabase&>(this->_database);
//
//		mongo_db.addClient();
//
//		return true;
//	}
//
//	return false;
//}

/*
	The functions check if the given database is MongoDB database and destroy the thread's client inside of it.
	Input: None;
	Output: If the destruction was successful - bool;
*/
//bool RequestHandlerFactory::destroyClientDB()
//{
//	if (this->_database.getClassType() == MongoDbDatabase::classType)
//	{
//		MongoDbDatabase& mongo_db = dynamic_cast<MongoDbDatabase&>(this->_database);
//
//		return mongo_db.deleteClient();;
//	}
//
//	return false;
//}

