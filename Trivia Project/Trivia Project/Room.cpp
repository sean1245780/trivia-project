#include "Room.h"

/*
	Initializes the Room class.
	Input: None;
	Output: None;
*/
Room::Room(const RoomData& data) : _metadata(data), _users()
{
}

Room::Room(Room&& other) noexcept
{
	this->_metadata = other._metadata;
	this->_users = other._users;
	other._metadata.id = 0;
	other._metadata.isActive = 0;
	other._metadata.maxPlayers = 0;
	other._metadata.numPlayers = 0;
	other._metadata.name = "";
	other._metadata.timePerQuestion = 0;
	this->_users.clear();
}

/*
	Destructs a room
	Input: None
	Output: None
*/
Room::~Room()
{
}

/*
	Adds a user to a room
	Input: None
	Output: None
*/
void Room::addUser(const LoggedUser& user)
{
	lock_guard<mutex> locking(this->_usersMutex);
	this->_users.push_back(user);
	this->_metadata.numPlayers++;
}

/*
	Removes a user from a room
	Input: None
	Output: None
*/
void Room::removeUser(const LoggedUser& user)
{
	lock_guard<mutex> locking(this->_usersMutex);
	
	for (auto it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if (it->getUsername() == user.getUsername())
		{
			this->_users.erase(it);
		}
	}
	this->_metadata.numPlayers--;

}

/*
	Gets all users in a room
	Input: None
	Output: A vector containing the names of all players
*/
vector<string> Room::getAllUsers()
{
	vector<string> players;
	lock_guard<mutex> locking(this->_usersMutex);

	for (auto user: this->_users)
	{
		players.push_back(user.getUsername());
	}

	return players;
}

RoomData Room::getData()
{
	this->_metadata.numPlayers = this->_users.size();
	return this->_metadata;
}
