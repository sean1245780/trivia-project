#include "LoginManager.h"

/*
	The function initizalizes the LoginManager class.
	Input: database = The database to initialize to - IDatabase*;
	Output: None;
*/
LoginManager::LoginManager(IDatabase& database) : _database(database)
{
}

/*
	The function destroys the LoginManager class.
	Input: None;
	Output: None;
*/
LoginManager::~LoginManager()
{
}

/*
	The function signs up the given user with the given paramters.
	Input: username = The username of the user - const string&; password = The password of the user - const string&; email = The email of the user - const string&;
	Output: None;
*/
void LoginManager::signup(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate)
{
	if (!std::regex_match(username, std::regex("^[a-zA-Z][a-zA-Z0-9]{2,24}$")))
		throw AnswerException(RSSC_ILLGL_USERN, RSSC_ILLGL_USERN_TXT);

	if (!std::regex_search(password, std::regex("^[a-zA-Z0-9\\!\\@\\#\\$\\%\\^\\&\\*]{8,40}$")))
		throw AnswerException(RSSC_ILLGL_PASSWRD, RSSC_ILLGL_PASSWRD_TXT);

	if (!std::regex_match(email, std::regex("^[a-zA-z][a-zA-Z0-9]{0,50}@[a-zA-z][a-zA-Z0-9]{0,30}(\\.(?!\\.)[a-zA-Z][a-zA-Z0-9]{0,30}){1,5}$"))) // validates the email
		throw AnswerException(RSSC_ILLGL_EMAIL, RSSC_ILLGL_EMAIL_TXT);
	
	if(!std::regex_match(address, std::regex("^[\\(][A-Za-z\\s]{1,30}, [0-9\\s]{1,30}, [A-Za-z\\s]{1,30}[\\)]$"))) // validates the address
		throw AnswerException(RSSC_ILLGL_ADDRS, RSSC_ILLGL_ADDRS_TXT);

	if (!std::regex_match(phone, std::regex("^[0]\\d{1,2}-\\d+$")))
		throw AnswerException(RSSC_ILLGL_PHONEN, RSSC_ILLGL_PHONEN_TXT);

	if(!std::regex_match(birthdate, std::regex("^([0][1-9]|[1-2]\\d|[3][0-1])/([0][1-9]|[1][0-2])/\\d{4}$")) && !std::regex_match(birthdate, std::regex("^([0][1-9]|[1-2]\\d|[3][0-1])\\.([0][1-9]|[1][0-2])\\.\\d{4}$")))
		throw AnswerException(RSSC_ILLGL_BRTHD_PRM, RSSC_ILLGL_BRTHD_PRM_TXT);

	if (!this->validateBirthdate(birthdate))
	{
		throw AnswerException(RSSC_ILLGL_BRTHD_PRM, RSSC_ILLGL_BRTHD_PRM_TXT);
	}

	if (this->_database.doesUserExist(username))
	{
		throw AnswerException(RSSC_ALRDY_TKN, RSSC_ALRDY_TKN_TXT);
	}

	this->_database.addNewUser(username, password, email, address, phone, birthdate);
	
	{
		lock_guard<mutex> locking(this->_loggedUsrsVecMutex);
		this->_loggedUsers.push_back(LoggedUser(username)); // No need to check because it checks if it is connected
	}
}

/*
	The function logins the given user with the given paramters.
	Input: username = The username of the user - const string&; password = The password of the user - const string&;
	Output: None;
*/
void LoginManager::login(const string& username, const string& password)
{
	if (!std::regex_match(username, std::regex("^[a-zA-Z][a-zA-Z0-9]{2,20}$")))
		throw AnswerException(RSSC_ILLGL_USERN, RSSC_ILLGL_USERN_TXT);

	if (!std::regex_search(password, std::regex("^[a-zA-Z0-9\\!\\@\\#\\$\\%\\^\\&\\*]{8,40}$")))
		throw AnswerException(RSSC_ILLGL_PASSWRD, RSSC_ILLGL_PASSWRD_TXT);


	if (!this->_database.doesUserExist(username, password))
		throw AnswerException(RSSC_WRONG_LOGIN_PRM, RSSC_WRONG_LOGIN_PRM_TXT);

	bool connected = false;

	{
		lock_guard<mutex> locking(this->_loggedUsrsVecMutex);
		for (std::vector<LoggedUser>::iterator it = this->_loggedUsers.begin(); it != this->_loggedUsers.end() && !connected; it++)
		{
			connected = username == it->getUsername();
		}

		if (!connected)
		{
			this->_loggedUsers.push_back(LoggedUser(username));
		}
		else
		{
			throw AnswerException(RSSC_USER_ALRDY_CNNCTD, RSSC_USER_ALRDY_CNNCTD_TXT);
		}
	}
}
/*
	Logs a user out
	Input: The username of the user to log out
	Output: None
*/
bool LoginManager::signout(const string& username)
{
	lock_guard<mutex> locking(this->_loggedUsrsVecMutex);

	for (auto it = this->_loggedUsers.begin();  it != this->_loggedUsers.end(); it++)
	{
		if (it->getUsername() == username)
		{
			this->_loggedUsers.erase(it);
			return true;
		}
	}

	return false;
}

vector<LoggedUser> LoginManager::getLoggedUsers()
{
	return this->_loggedUsers;
}

/*
	This function checks if the birthdate fits the current time.
	Input: birthdate = The birth date to check - const std::string&; 
	Output: Whether the input is valid
*/
bool LoginManager::validateBirthdate(const std::string& birthdate)
{
	bool doting = birthdate.find('.') != string::npos;
	bool slashing = birthdate.find('/') != string::npos;

	if ((doting && slashing) || (!doting && !slashing))
	{
		return false;
	}

	time_t nowing = time(0);
	tm time_now{ 0 };

	if (localtime_s(&time_now, &nowing))
	{
		throw std::exception("Error occured in time checking!");
	}

	time_now.tm_year += MIN_YEAR;
	time_now.tm_mon++;

	int day = 0, month = 0, year = 0;

	if (doting)
	{
		sscanf_s(birthdate.c_str(), "%d.%d.%d", &day, &month, &year);
	}
	else if (slashing)
	{
		sscanf_s(birthdate.c_str(), "%d/%d/%d", &day, &month, &year);
	}
	else
	{
		return false;
	}

	if (year < MIN_YEAR) 
	{ return false; }

	bool year_invalid = time_now.tm_year < year;
	bool month_invalid = time_now.tm_mon < month;
	bool day_invalid = time_now.tm_mday < day;

	// If the current time is more than the one that was given
	if (year_invalid || (time_now.tm_year == year && month_invalid) || (time_now.tm_year == year && time_now.tm_mon == month && day_invalid))
	{ return false; }

	// Days and months validation
	if (((year % 4 != 0 && month == 2 && day > 28) || (year % 4 == 0 && month == 2 && day > 29)) || (month >= 1 && month <= 7 && month % 2 == 0 && day > 30) || (month >= 8 && month <= 12 && month % 2 != 0 && day > 30))
	{ return false; }



	return true;
}