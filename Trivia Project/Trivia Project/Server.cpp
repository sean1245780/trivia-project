#include "Server.h"

//pool* Server::getPoolInitialized()
//{
//	if (this->_database.getClassType() == MongoDbDatabase::classType)
//	{
//		MongoDbDatabase& mongodb_db = dynamic_cast<MongoDbDatabase&>(this->_database);
//		return (&mongodb_db.getClientsPool());
//	}
//
//	return nullptr;
//}

/*
	The function initializes the Server class.
	Input: None;
	Output: None;
*/
Server::Server() : _server_working(false), _database(MongoDbDatabase::getInstance()), _handlerFactory(RequestHandlerFactory::getInstance<IDatabase&>(this->_database)), _communicator(Communicator::getInstance<RequestHandlerFactory&>(this->_handlerFactory))
{}

/*
	The function desto
	Input: server = The server to delete - void*;
	Ouput: None;
*/
Server::~Server()
{
	if (this->_server_working) { this->_communicator.communication_work = false; }
	this->_server_working = false;
}

/*
	The function runs the server and openes to listening.
	Input: None;
	Output: None;
*/
void Server::run()
{
	_server_working = true;
	this->_communicator.communication_work = true;

	std::thread t_connector(&Communicator::startHandleRequests, std::ref(_communicator)); // Thread for the communication
	t_connector.detach();

	std::string val = "";
	while (_server_working) // Admin command option
	{
		
		coutts << "Enter your command: " << std::endl;
		
		std::cin >> val;

		if (val == CSRVR_EXIT) { _server_working = false; }
	}

	this->_communicator.communication_work = false;
}
