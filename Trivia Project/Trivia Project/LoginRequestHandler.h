#pragma once

// Constants Defines
#define RQSC
#define RSSC

#include "IRequestHandler.h"
#include "Constants.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "AnswerException.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
private:
	// Variables
	RequestHandlerFactory& _handlerFactory;

	// Functions & Methods
	RequestResult login(const RequestInfo& request);
	RequestResult signup(const RequestInfo& request);

public:
	// Constructors & Destructors
	LoginRequestHandler(RequestHandlerFactory& handlerFactory);
	virtual ~LoginRequestHandler();

	// Functions & Methods
	virtual bool isRequestRelevant(RequestInfo info);
	virtual RequestResult handleRequest(RequestInfo info);
	virtual bool disconnectUser();
};

