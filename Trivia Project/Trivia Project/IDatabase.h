#pragma once

#include <string>
#include <unordered_set>
#include "json.hpp"
#include "Constants.h"

using std::string;
using std::unordered_set;
using namespace nlohmann;

class IDatabase
{
protected:
	// Constructors
	IDatabase() = default;

public:
	virtual ~IDatabase() = default;
	virtual bool doesUserExist(const string& username) = 0;
	virtual bool doesUserExist(const string& username, const string& password) = 0;
	virtual bool doesPasswordMatch(const string& password) = 0;
	virtual void addNewUser(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate) = 0;
	virtual unordered_set<string> getQuestions(const int amount) = 0;
	virtual double getPlayerAverageAnswerTime(const string& usrname) = 0;
	virtual int getNumOfCorrectAnswers(const string& username) = 0;
	virtual int getNumOfTotalAnswers(const string& username) = 0;
	virtual int getNumOfPlayerGames(const string& username) = 0;
	virtual json getTopUserStatistics(const int limit) = 0;
	virtual json getUserStatistics(const string& username) = 0;
	virtual typeDBClasses getClassType() const = 0;
};