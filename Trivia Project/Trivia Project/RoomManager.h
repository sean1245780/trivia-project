#pragma once

#define RSSC
#define RMNGMNT

#include <mutex>
#include <unordered_map>
#include <vector>
#include "SingletonTemplate.h"
#include "Room.h"
#include "LoggedUser.h"
#include "AnswerException.h"
#include "Constants.h"

using std::unordered_map;
using std::vector;
using std::mutex;
using std::unique_lock;
using std::lock_guard;

class RoomManager : public SingletonTemplate<RoomManager>
{
private:
	// Variables
	unordered_map<unsigned int, Room> _rooms;
	int _nextRoomId;
	mutex _roomsMutex;

	//Constructor
	RoomManager() : _nextRoomId(0) {};

	friend class SingletonTemplate;

public:
	virtual ~RoomManager();

	void createRoom(const LoggedUser& user, const RoomData& data);
	void deleteRoom(const unsigned int roomId);
	unsigned_byte getRoomState(const unsigned int roomId);
	vector<RoomData> getRooms();
	unordered_map<unsigned int, Room>& getAllRooms();
	int getNextId();

};

