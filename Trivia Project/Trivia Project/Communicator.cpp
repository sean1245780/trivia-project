#include "Communicator.h"

/*
	Initializes the Communicator class
	Input: the socket, port and whether the server is binding or handling]
	Output: None
*/
Communicator::Communicator(RequestHandlerFactory& handlerFactory, const int port) : _binding(false), _handling(false), _port(port), _serverSocket(0), _handlerFactory(handlerFactory),
	communication_work(false), _threadCount(0)
{
}

/*
	Destructs the Communicator class and closes the socket
	Input: None
	Output: None
*/
Communicator::~Communicator()
{
	this->communication_work = false;

	for (auto it = this->_clients.begin(); it != this->_clients.end(); it++)
	{
		closesocket(it->first);
	}

	while (this->_threadCount > 0);

	if (_binding)
	{
		try
		{
			// the only use of the destructor should be for freeing 
			// resources that was allocated in the constructor
			closesocket(_serverSocket);
		}
		catch (...) {}
	}
}

/*
	Handles a new client connection and sends him data
	Input: The client socket
	Output: None
*/
void Communicator::handleNewClient(const SOCKET clientSocket)
{
	// First of all
	++this->_threadCount;
	char* data = nullptr, * status = nullptr, * length = nullptr;
	std::string username;
	int lengthNum = 0;
	RequestResult res;
	IRequestHandler* requestHandler = nullptr;
	unique_lock<mutex>locking(this->_clientsMapMutex, std::defer_lock);

	locking.lock();
	requestHandler = this->_clients.at(clientSocket);
	locking.unlock();
	
	coutts << "Client connected -> " << clientSocket << std::endl;

	//coutts << "MongoDB client initialization: " << ((this->_handlerFactory.initializeClientDB()) ? ("true") : ("false")) << std::endl;

	try
	{
		while (Communicator::communication_work)
		{
			status = getData(clientSocket, STATUS_SIZE);
			length = getData(clientSocket, LENGTH_SIZE);
			
			if (status == nullptr || length == nullptr) { throw std::exception((string("Invalid data receiving! -> ") += std::to_string(clientSocket)).c_str()); }

			lengthNum = (*(int*)length);

			data = getData(clientSocket, lengthNum);

			if (!Communicator::communication_work) { throw std::exception((string("Invalid socket existance, server was shutdown! -> ") += std::to_string(clientSocket)).c_str()); }

			RequestInfo info;
			time(&info.recivalTime);
			memcpy(&info.id, status, STATUS_SIZE);
			info.buffer.append(data, lengthNum);

			//std::cout << "Socketing: " << (unsigned int)info.id << " + " << info.buffer << " + " << info.recivalTime << std::endl;

			delete[] status;
			delete[] length;
			if (data != nullptr) { delete[] data; }

			status = nullptr;
			length = nullptr;
			data = nullptr;

			


			if (requestHandler->isRequestRelevant(info))
			{
				res = requestHandler->handleRequest(info);

				if (res.newHandler != nullptr)
				{
					delete requestHandler;
					requestHandler = res.newHandler;
				}

				sendData(clientSocket, res.response);

			}
			else
			{
				sendData(clientSocket, JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ RSSC_ERR_MSG_REQST_RELEVANT }, RSSC_ERROR));
			}
		}
	}
	catch(const std::exception& e)
	{
		coutts << "Error occurred on socket: " << clientSocket << " -> " << e.what() << std::endl;
	}
	
	closesocket(clientSocket);
	
	bool disconnect = requestHandler->disconnectUser();
	coutts << "The user that disconnected on socket: " << clientSocket << " -> Was: " << ((disconnect) ? ("A Logged In User!") : ("An Anonymous User!")) << std::endl;

	//coutts << "MongoDB client destruction: " << ((this->_handlerFactory.destroyClientDB()) ? ("true") : ("false")) << std::endl;

	locking.lock();
	this->_clients.erase(clientSocket);
	locking.unlock();

	coutts << "Client disconnected and closed the socket -> " << clientSocket << std::endl;

	this->_threadCount--;
}

/*
	Sends data through a socket to the client
	Input: the socket and the data to send
	Output: None
*/
void Communicator::sendData(SOCKET socket, string data)
{
	const char* msg = data.c_str();

	int res = send(socket, msg, data.size(), 0);

	if (res == INVALID_SOCKET || res == SOCKET_ERROR || !res)
	{
		std::string error = "Error while sending message to client: ";
		error += std::to_string(socket);
		throw std::exception(error.c_str());
	}
}

/*
	Gets data from the user 
	Input: The socket, the size of the input and flags
	Output: The data
*/
char* Communicator::getData(SOCKET socket, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum]();
	
	int res = recv(socket, data, bytesNum, flags);

	if (res == INVALID_SOCKET || res == SOCKET_ERROR || !res)
	{
		std::string error = "Error while recieving from socket: ";
		error += std::to_string(socket);
		throw std::exception(error.c_str());
	}

	return data;
}

/*
	Binds the socket to the port and listens to connections
	Input: None
	Output: None
*/
void Communicator::bindAndListen()
{
	if (_binding) { return; }
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(this->_port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	_binding = true;
}

/*
	Initializes the server socket and gets the connections
	Input: None
	Output: None
*/
void Communicator::startHandleRequests()
{
	if (_handling) { return; }

	if (!_binding)
	{
		bindAndListen();
	}

	_handling = true;

	while (communication_work)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		SOCKET client_socket = accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__" -> Client socket wasn't able to connect!");

		std::thread t(&Communicator::handleNewClient, this, client_socket);
		t.detach();

		{
			lock_guard<mutex> locking(this->_clientsMapMutex);
			this->_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, this->_handlerFactory.createLoginRequestHandler()));
		}
	}

}