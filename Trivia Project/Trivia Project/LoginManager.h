#pragma once

// Constants Defines
#define DBUSE
#define RSSC

#include <vector>
#include <mutex>
#include <algorithm>
#include <regex>
#include "IDatabase.h"
#include "LoggedUser.h"
#include "SingletonTemplate.h"
#include "AnswerException.h"
#include "Constants.h"

using std::vector;
using std::mutex;
using std::lock_guard;

class LoginManager : public SingletonTemplate<LoginManager>
{
private:
	// Variables
	IDatabase& _database;
	vector<LoggedUser> _loggedUsers;
	mutex _loggedUsrsVecMutex;
	
	// Contructors
	LoginManager(IDatabase& database);

	// Functions
	bool validateBirthdate(const std::string& password);

	// Class Workout
	friend class SingletonTemplate;

public:
	virtual ~LoginManager(); 

	// Functions & Methods
	void signup(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate);
	void login(const string& username, const string& password);
	bool signout(const string& username);
	vector<LoggedUser> getLoggedUsers();
};

