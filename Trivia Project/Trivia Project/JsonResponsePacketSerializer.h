#pragma once

// Constants Defines
#define JRPS 
#define RSSC 

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include "json.hpp"
#include "Constants.h"
#include "Room.h"

using std::string;
using std::stringstream;
using std::vector;
using json = nlohmann::json;

typedef struct LoginResponse
{
public:
	unsigned_byte status;

} LoginResponse;

typedef struct SignupResponse
{
public:
	unsigned_byte status;

} SignupResponse;

typedef struct ErrorResponse
{
public:
	string message;

} ErrorResponse;

typedef struct LogoutResponse
{
public:
	unsigned_byte status;

} LogoutResponse;

typedef struct JoinRoomResponse
{
public:
	unsigned_byte status;

} JoinRoomResponse;

typedef struct GetRoomsResponse
{
public:
	unsigned_byte status;
	vector<RoomData> rooms;

} GetRoomsResponse;

typedef struct CreateRoomResponse
{
public:
	unsigned_byte status;

} CreateRoomResponse;

typedef struct GetPlayersInRoomResponse
{
public:
	unsigned_byte status;
	vector<string> players;

} GetPlayersInRoomResponse;

typedef struct GetStatisticsResponse
{
public:
	unsigned_byte status;
	json statistics;

} GetStatisticsResponse;

class JsonResponsePacketSerializer
{
private:
	// Constructors
	JsonResponsePacketSerializer();
	
	// Methods & Functions
	static string parseBuffer(const json& vals, const unsigned_byte code);

public:
	// Methods & Functions
	static string serializeResponse(const ErrorResponse& error_resp, const unsigned_byte code_status);
	static string serializeResponse(const LoginResponse& login_resp, const unsigned_byte code_status);
	static string serializeResponse(const SignupResponse& signup_resp, const unsigned_byte code_status);
	static string serializeResponse(const LogoutResponse& logout_resp, const unsigned_byte code_status);
	static string serializeResponse(const GetRoomsResponse& getRoomResponse_resp, const unsigned_byte code_status);
	static string serializeResponse(const GetPlayersInRoomResponse& getPlayersInRoom_resp, const unsigned_byte code_status);
	static string serializeResponse(const JoinRoomResponse& joinRoom_resp, const unsigned_byte code_status);
	static string serializeResponse(const CreateRoomResponse& createRoom_resp, const unsigned_byte code_status);
	static string serializeResponse(const GetStatisticsResponse& getStatistics_resp, const unsigned_byte code_status);
};

