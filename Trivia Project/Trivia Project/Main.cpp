#pragma comment (lib, "ws2_32.lib")

#include "Server.h"

int main()
{
	Server& server = Server::getInstance(); // Creating the instance and getting it
	server.run(); // Run the server

	return 0;
}