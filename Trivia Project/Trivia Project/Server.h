#pragma once

// Constants Defines
#define CSRVR

#include "Constants.h"
#include "Communicator.h"
#include "IDatabase.h"
#include "SqliteDatabase.h"
#include "MongoDbDatabase.h"
#include "RequestHandlerFactory.h"
#include "SingletonTemplate.h"


class Server : public SingletonTemplate<Server>
{
private:
	// Variables
	IDatabase& _database;
	RequestHandlerFactory& _handlerFactory;
	Communicator& _communicator;
	bool _server_working;

	// Constructors
	Server();

	// Class Workout
	friend class SingletonTemplate;

public:
	// Constructors & Destructors
	virtual ~Server();
	
	// Functions & Methods
	void run();
};

