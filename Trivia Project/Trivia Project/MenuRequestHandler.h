#pragma once

#define RQSC
#define RSSC
#define STTSMNGR

#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "Constants.h"

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{

private:
	LoggedUser _loggedUser;
	RequestHandlerFactory& _requestHandlerFactory;
	RequestResult signout(const RequestInfo& info);
	RequestResult getRooms(const RequestInfo& info);
	RequestResult getPlayersInRoom(const RequestInfo& info);
	RequestResult getStatisticsUsr(const RequestInfo& info);
	RequestResult getStatisticsTopUsrs(const RequestInfo& info);
	RequestResult joinRoom(const RequestInfo& info);
	RequestResult createRoom(const RequestInfo& info);

public:
	MenuRequestHandler(LoggedUser loggedUser, RequestHandlerFactory& requestHandlerFactory) : _loggedUser(loggedUser), _requestHandlerFactory(requestHandlerFactory) {}
	virtual ~MenuRequestHandler();
	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult handleRequest(RequestInfo info) override;
	virtual bool disconnectUser() override;
};

