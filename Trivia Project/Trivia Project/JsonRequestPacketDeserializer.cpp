#include "JsonRequestPacketDeserializer.h"

/*
	Desirializes a login request from bytes to a struct
	Input: The message
	Ouput: A login request
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const string& buffer)
{
	LoginRequest loginRequest;
	json json_msg = json::parse(buffer);

	try
	{
		loginRequest.username = json_msg.at(JRPD_USERNAME_TXT).dump();
		loginRequest.password = json_msg.at(JRPD_PASSWORD_TXT).dump();

		loginRequest.username = loginRequest.username.substr(1, loginRequest.username.size() - 2);
		loginRequest.password = loginRequest.password.substr(1, loginRequest.password.size() - 2);
	}
	catch (const std::exception& e)
	{
		throw std::exception((stringstream() <<"Invalid json parsing in desirializer! -> " << __FUNCTION__ << "\n\tAbouve Error: " << e.what()).str().c_str());
	}

	return loginRequest;
}

/*
	Desirializes a signup request from bytes to a struct
	Input: The message
	Output: A signup request
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const string& buffer)
{
	SignupRequest signupRequest;
	json json_msg = json::parse(buffer);

	try
	{
		signupRequest.username = json_msg.at(JRPD_USERNAME_TXT).dump();
		signupRequest.password = json_msg.at(JRPD_PASSWORD_TXT).dump();
		signupRequest.email = json_msg.at(JRPD_EMAIL_TXT).dump();
		signupRequest.address = json_msg.at(JRPD_ADDRESS_TXT).dump();
		signupRequest.phone_num = json_msg.at(JRPD_PHONE_TXT).dump();
		signupRequest.birth_date = json_msg.at(JRPD_BIRTH_DATE_TXT).dump();

		signupRequest.username = signupRequest.username.substr(1, signupRequest.username.size() - 2);
		signupRequest.password = signupRequest.password.substr(1, signupRequest.password.size() - 2);
		signupRequest.email = signupRequest.email.substr(1, signupRequest.email.size() - 2);
		signupRequest.address = signupRequest.address.substr(1, signupRequest.address.size() - 2);
		signupRequest.phone_num = signupRequest.phone_num.substr(1, signupRequest.phone_num.size() - 2);
		signupRequest.birth_date = signupRequest.birth_date.substr(1, signupRequest.birth_date.size() - 2);
	}
	catch (const std::exception& e)
	{
		throw std::exception((stringstream() << "Invalid json parsing in desirializer! -> " << __FUNCTION__ << "\n\tAbouve Error: " << e.what()).str().c_str());
	}

	return signupRequest;
}
/*
	Desirializes a buffer into a get players in a room request
	Input: a buffer
	Output: A request to get all players in a room
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const string& buffer)
{

	GetPlayersInRoomRequest getPlayersRequest;
	json jsonMsg = json::parse(buffer);

	try
	{ 
		getPlayersRequest.roomId = (unsigned int)std::strtoul(strToNums(jsonMsg.at(JRPD_ROOM_ID_TXT).dump().c_str()).c_str(), nullptr, 10);
	}
	catch (const std::exception& e)
	{
		throw std::exception((stringstream() << "Invalid json parsing in desirializer! -> " << __FUNCTION__ << "\n\tAbouve Error: " << e.what()).str().c_str());
	}

	return getPlayersRequest;
}

/*
	Desirializes a buffer into a request to join a room
	Input: a buffer
	Output: A request to join a room
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const string& buffer)
{

	JoinRoomRequest joinRoomRequest;
	json jsonMsg = json::parse(buffer);

	try
	{
		joinRoomRequest.roomId = (unsigned int)std::strtoul(strToNums(jsonMsg.at(JRPD_ROOM_ID_TXT).dump().c_str()).c_str(), nullptr, 10);
	}
	catch (const std::exception& e)
	{
		throw std::exception((stringstream() << "Invalid json parsing in desirializer! -> " << __FUNCTION__ << "\n\tAbouve Error: " << e.what()).str().c_str());
	}
	return joinRoomRequest;
}

/*
	Desirializes a buffer into a request to create a room
	Input: A buffer
	Output: A request to create a room
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const string& buffer)
{

	CreateRoomRequest createRoomRequest;
	json jsonMsg = json::parse(buffer);

	try 
	{
		createRoomRequest.answerTimeout = (unsigned int)std::strtoul(strToNums(jsonMsg.at(JRPD_ANSWER_TIMEOUT_TXT).dump().c_str()).c_str(), nullptr, 10);
		createRoomRequest.maxUsers = (unsigned int)std::strtoul(strToNums(jsonMsg.at(JRPD_MAX_USERS_TXT).dump().c_str()).c_str(), nullptr, 10);
		createRoomRequest.questionCount = (unsigned int)std::strtoul(strToNums(jsonMsg.at(JRPD_QUESTION_COUNT_TXT).dump().c_str()).c_str(), nullptr, 10);
		createRoomRequest.roomName = jsonMsg.at(JRPD_ROOM_NAME_TXT).dump();

		createRoomRequest.roomName = createRoomRequest.roomName.substr(1, createRoomRequest.roomName.size() - 2);
	}
	catch (const std::exception& e)
	{
		throw std::exception((stringstream() << "Invalid json parsing in desirializer! -> " << __FUNCTION__ << "\n\tAbouve Error: " << e.what()).str().c_str());
	}

	return createRoomRequest;
}

string JsonRequestPacketDeserializer::strToNums(string val)
{
	size_t pos = -1;
	while ((pos = val.find("\"")) != -1)
	{
		val = val.erase(pos, pos + 1);
	}

	return val;
}
