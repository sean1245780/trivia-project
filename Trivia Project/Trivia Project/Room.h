#pragma once

#include <string>
#include <vector>
#include <mutex>
#include "Constants.h"
#include "LoggedUser.h"

using std::string;
using std::vector;
using std::mutex;
using std::unique_lock;
using std::lock_guard;

typedef struct RoomData
{
public:
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int numPlayers;
	unsigned int timePerQuestion;
	unsigned int numQuestions;
	unsigned_byte isActive;

	RoomData() : id(0), name(""), maxPlayers(0), numPlayers(0), timePerQuestion(0),
		numQuestions(0), isActive(RST_INIT_MODE) {}

} RoomData;


class Room
{
private:
	//Variables
	RoomData _metadata;
	vector<LoggedUser> _users;
	mutex _usersMutex;

public:
	// Constructor & Destructor
	Room() = default;
	Room(const RoomData& data);
	Room(Room&& other) noexcept;
	~Room();

	//Functions
	void addUser(const LoggedUser& user);
	void removeUser(const LoggedUser& user);	
	vector<string> getAllUsers();
	RoomData getData();
};

