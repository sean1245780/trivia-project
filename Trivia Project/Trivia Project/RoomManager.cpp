#include "RoomManager.h"

/*
	Destructs a room manager
	Input: None 
	Output: None
*/
RoomManager::~RoomManager()
{
}

/*
	Creates a room
	Input: The user that creates the room
	Output: None
*/
void RoomManager::createRoom(const LoggedUser& user, const RoomData& data)
{
	lock_guard<mutex> locking(this->_roomsMutex);
	unsigned int numing = data.id;

	if (data.maxPlayers < RMNGMNT_MIN_USERS || data.maxPlayers > RMNGMNT_MAX_USERS ||
		data.numQuestions < RMNGMNT_MIN_QUESTIONS || data.numQuestions > RMNGMNT_MAX_QUESTIONS ||
		data.timePerQuestion < RMNGMNT_MIN_TIME || data.timePerQuestion > RMNGMNT_MAX_TIME || data.name.size() > RMNGMNT_MAX_NAME_LEN ||
		data.name.size() < RMNGMNT_MIN_NAME_LEN)
	{
		throw AnswerException(RSSC_CREATE_ROOM, "Invalid given parameters!");
	}

	auto ans = this->_rooms.insert(std::make_pair(numing, Room(data)));
	if (!ans.second) { throw AnswerException(RSSC_CREATE_ROOM, "Invalid room creation ans insertion!"); }
	
	try
	{
		this->_rooms.at(data.id).addUser(user);
	}
	catch (const std::exception& e)
	{
		this->_rooms.erase(numing);
		throw AnswerException(RSSC_CREATE_ROOM, (string("Invalid room creation and admin user adding! -> ") += e.what()));
	}

	this->_nextRoomId++;
}

/*
	Deletes a room
	Input: The room id to delete
	Output: None
*/
void RoomManager::deleteRoom(const unsigned int roomId)
{
	lock_guard<mutex> locking(this->_roomsMutex);

	unsigned int ans = this->_rooms.erase(roomId);

	if (!ans) { throw AnswerException(RSSC_ERR_ROOM_NOT_EXST, ("Invalid deletion of room by the given id! ->" + std::to_string(roomId))); }
}

/*
	Gets the state of a certain room
	Input: The room id
	Output: The room state
*/
unsigned_byte RoomManager::getRoomState(const unsigned int roomId)
{
	unsigned_byte state = 0;
	lock_guard<mutex> locking(this->_roomsMutex);

	try
	{
		state = this->_rooms.at(roomId).getData().isActive;
	}
	catch (const std::exception& e)
	{
		throw AnswerException(RSSC_ERR_ROOM_NOT_EXST, string("Invalid data getting at the given room id! -> ") += e.what());
	}

	return state;
}


/*
	Gets all of the rooms
	Input: None
	Output: All of the rooms data
*/
vector<RoomData> RoomManager::getRooms()
{
	vector<RoomData> rooms;
	lock_guard<mutex> locking(this->_roomsMutex);

	for (auto it = this->_rooms.begin(); it != this->_rooms.end(); it++)
	{
		rooms.push_back(it->second.getData());
	}

	return rooms;
}

int RoomManager::getNextId()
{
	return this->_nextRoomId;
}

unordered_map<unsigned int, Room>& RoomManager::getAllRooms()
{
	return this->_rooms;
}


