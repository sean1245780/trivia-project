#pragma once

#include <string>

using std::string;

class LoggedUser
{
private:
	string _username;

public:
	// Constructors & Destructors
	LoggedUser(const string& username);

	// Getters & Setters
	string getUsername() const;
};

