#include "JsonResponsePacketSerializer.h"

/*
	The function parser the given json package to buffer.
	Input: vals = The json object that holds the needed data to parse - const json&; code = The code of the message by the given protocol - const unsigned_byte;
	Output: The buffer after parsing by the protocol - std::string;
*/
string JsonResponsePacketSerializer::parseBuffer(const json& vals, const unsigned_byte code_status)
{
	string ans = "";
	string resp_buff = vals.dump();
	unsigned char* resp_size_buff = new unsigned char[sizeof(int)]();
	(*((int*)resp_size_buff)) = resp_buff.size();

	ans += code_status;
	for (int i = 0; i < sizeof(int); i++)
	{
		ans += resp_size_buff[i];
	}

	ans += resp_buff;
	delete[] resp_size_buff;

	return ans;
}

/*
	The function serializes the given error response.
	Input: error_resp = The error response that will get parsed - const ErrorResponse&;
	Output: The buffer after parsing by the protocol - std::string;
*/
string JsonResponsePacketSerializer::serializeResponse(const ErrorResponse& error_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_MSG_TXT, error_resp.message} };
	return parseBuffer(json_msg, code_status);
}

/*
	The function serializes the given login response.
	Input: login_resp = The login response that will get parsed - const LoginResponse&;
	Output: The buffer after parsing by the protocol - std::string;
*/
string JsonResponsePacketSerializer::serializeResponse(const LoginResponse& login_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, login_resp.status} };
	return parseBuffer(json_msg, code_status);
}

/*
	The function serializes the given signup response.
	Input: signup_resp = The signup response that will get parsed - const SignupResponse&;
	Output: The buffer after parsing by the protocol - std::string;
*/
string JsonResponsePacketSerializer::serializeResponse(const SignupResponse& signup_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, signup_resp.status} };
	return parseBuffer(json_msg, code_status);
}

/*
	Serializes a logout response
	Input: The response and its status
	Output: A buffer containing the response
*/
string JsonResponsePacketSerializer::serializeResponse(const LogoutResponse& logout_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, logout_resp.status} };
	return parseBuffer(json_msg, code_status);
}
/*
	Serializes a rooms response into a buffer
	Input: The response and its status 
	Output: A buffer containing the response
*/
string JsonResponsePacketSerializer::serializeResponse(const GetRoomsResponse& getRoomsResponse_resp, const unsigned_byte code_status)
{
	json json_msg = json({});
	json_msg[JRPS_STATUS_TXT] = getRoomsResponse_resp.status;
	json_msg[JRPS_DATA_TXT] = json::array();

	for (auto it = getRoomsResponse_resp.rooms.begin(); it != getRoomsResponse_resp.rooms.end(); it++)
	{
		json room_data = { {JRPS_ID_TXT, it->id}, {JRPS_NAME_TXT, it->name}, {JRPS_MAX_PLAYERS_TXT, it->maxPlayers}, {JRPS_NUM_PLAYERS_TXT, it->numPlayers},
							{JRPS_QUESTION_COUNT_TXT, it->numQuestions}, {JRPS_TIME_PER_QUESTION_TXT, it->timePerQuestion}, {JRPS_IS_ACTIVE_TXT, it->isActive} };
		json_msg[JRPS_DATA_TXT].push_back(room_data);
	}

	return parseBuffer(json_msg, code_status);
}

/*
	Serializes a response that gets all players in a room
	Input: The response and its status
	Output: A buffer containing the response
*/
string JsonResponsePacketSerializer::serializeResponse(const GetPlayersInRoomResponse& getPlayersInRoom_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, getPlayersInRoom_resp.status}, {JRPS_DATA_TXT, json(getPlayersInRoom_resp.players)} };
	return parseBuffer(json_msg, code_status);
}

/*
	Serializes a response that creates a room
 	Input: The response and its status
	Output: A buffer containing the response
*/
string JsonResponsePacketSerializer::serializeResponse(const JoinRoomResponse& joinRoom_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, joinRoom_resp.status} };
	return parseBuffer(json_msg, code_status);
}

/*
	Serializes a response that creates a room
	Input: The response and its status
	Output: A buffer containing the response
*/
string JsonResponsePacketSerializer::serializeResponse(const CreateRoomResponse& createRoom_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, createRoom_resp.status} };
	return parseBuffer(json_msg, code_status);
}

/*
	Serializes a statistics response
	Input: The response and its status
	Output: A buffer containing the response
*/
string JsonResponsePacketSerializer::serializeResponse(const GetStatisticsResponse& getStatistics_resp, const unsigned_byte code_status)
{
	json json_msg = { {JRPS_STATUS_TXT, getStatistics_resp.status}, {JRPS_DATA_TXT, getStatistics_resp.statistics} };
	return parseBuffer(json_msg, code_status);
}
