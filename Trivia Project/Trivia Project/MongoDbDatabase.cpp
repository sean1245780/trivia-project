#include "MongoDbDatabase.h"

const typeDBClasses MongoDbDatabase::classType = ID_MONGODB_DB;

/*
	The function intializes the MongoDbDatabase class.
	Input: None;
	Output: None;
*/
MongoDbDatabase::MongoDbDatabase() : _instance{}, _clientsPool{ _uriConnection },
	_uriConnection{/*(stringstream() << DBUSE_MONGODB_DEFAULT_URI << "/?" DBUSE_MONGODB_MIN_SIZE_POOL << "&" << DBUSE_MONGODB_MAX_SIZE_POOL).str()*/}
{}

/*
	The function aquaires a client from the clients pool by the wanted thread if needed.
	Input: None;
	Output: The client to use - pool::entry&;
*/
//client& MongoDbDatabase::_acquireClient(const string& db_name)
//{
//	thread::id threadsId = std::this_thread::get_id();
//	unique_lock<mutex> locking(_clientsMutex, std::defer_lock);
//
//	locking.lock();
//	auto threadItter = this->_clients.find(threadsId);
//	locking.unlock();
//
//	if (threadItter == this->_clients.end())
//	{
//		if (db_name.empty())
//		{
//			locking.lock();
//			auto pos = this->_clients.insert(std::pair<thread::id, clientEntry>(threadsId, clientEntry(this->_clientsPool)));
//			return pos.first->second.setClient(this->_clientsPool);
//		}
//		else
//		{
//			locking.lock();
//			auto pos = this->_clients.insert(std::pair<thread::id, clientEntry>(threadsId, clientEntry(this->_clientsPool, db_name)));
//			return pos.first->second.setClient(this->_clientsPool);
//		}
//	}
//
//	locking.lock();
//	return this->_clients.at(threadsId).getClient();
//}
//
//database& MongoDbDatabase::_acquireDatabase(const string& db_name)
//{
//	thread::id threadsId = std::this_thread::get_id();
//	unique_lock<mutex> locking(_clientsMutex, std::defer_lock);
//
//	locking.lock();
//	auto threadItter = this->_clients.find(threadsId);
//	locking.unlock();
//
//	if (threadItter == this->_clients.end())
//	{
//		if (!db_name.empty())
//		{
//			locking.lock();
//			auto pos = this->_clients.insert(std::pair<thread::id, clientEntry>(threadsId, clientEntry(this->_clientsPool, db_name)));
//			return pos.first->second.getDatabase();
//		}
//		else
//		{
//			throw std::runtime_error(__FUNCTION__" -> Invalid database name was given!");
//		}
//	}
//
//	locking.lock();
//	auto& objct = this->_clients.at(threadsId);
//
//	return ((objct.hasDatabase()) ? (objct.getDatabase()) : (objct.setDatabase(db_name)));
//}

/*
	The function destroys the MongoDbDatabase class.
	Input: None;
	Output: None;
*/
MongoDbDatabase::~MongoDbDatabase() {}

/*
	The function finds out if the user exists in the database.
	Input: username = User's username to look by - const string&;
	Output: If user by this username exists - bool;
*/
bool MongoDbDatabase::doesUserExist(const string& username)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result = col.find_one(document{} << DBUSE_USERNAME_TXT << username << finalize);

	return ((maybe_result) ? (true) : (false));
}

/*
	The function finds out if the user exists in the database.
	Input: username = User's username to look by - const string&; password = User's password to look by - const string&;
	Output: If user by this username and password exists - bool;
*/
bool MongoDbDatabase::doesUserExist(const string& username, const string& password)
{
	//auto client = this->_clientsPool.acquire();
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result = col.find_one(document{} << "$and" << open_array << document{} << open_document << DBUSE_USERNAME_TXT << username << close_document <<
			document{} << open_document << DBUSE_PASSWORD_TXT << password << close_document << close_array << finalize);
	
	// In a situation it doesn't work
	/*
		bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
		col.find_one(make_document(kvp("$and", make_array(make_document(kvp(DBUSE_USERNAME_TXT, username)), make_document(kvp(DBUSE_PASSWORD_TXT, password))))));
	*/

	return ((maybe_result) ? (true) : (false));
}

/*
	The function finds out if there is user with the given password.
	Input: password = User's password to look by - const string&;
	Output: If user by this password exists - bool;
*/
bool MongoDbDatabase::doesPasswordMatch(const string& password)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];
	bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result;

	{
		lock_guard<mutex> locking(this->_queryMutex);
		maybe_result = col.find_one(document{} << DBUSE_PASSWORD_TXT << password << finalize);
	}

	return ((maybe_result) ? (true) : (false));
}

/*
	The function adds new user to the data base.
	Input: username = User's username to add - const string&; password = User's password to add - const string&; email = User's email to add - const string&;
	Output: None;
*/
void MongoDbDatabase::addNewUser(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	auto builder = bsoncxx::builder::stream::document{};
	bsoncxx::document::value doc_value = builder
		<< DBUSE_USERNAME_TXT << username
		<< DBUSE_PASSWORD_TXT << password
		<< DBUSE_EMAIL_TXT << email
		<< DBUSE_ADDRESS_TXT << address
		<< DBUSE_PHONE_TXT << phone
		<< DBUSE_BIRTH_DATE_TXT << birthdate
		<< DBUSE_STATISTICS_TBL_NAME << open_document
		<< DBUSE_STATISTICS_NUM_GAMES << 0
		<< DBUSE_STATISTICS_CRCT_ANS << 0 << DBUSE_STATISTICS_WRONG_ANS << 0
		<< DBUSE_STATISTICS_AVG_TIME_PER_QSTION << 0 << DBUSE_STATISTICS_SCORE_POINTS << 0
		<< close_document << bsoncxx::builder::stream::finalize;

	bsoncxx::stdx::optional<mongocxx::result::insert_one> result;

	{
		lock_guard<mutex> locking(this->_queryMutex);
		result = col.insert_one(doc_value.view());
	}
}

/*
	Gets a specific amount of questions from the database
	Input: The amount of question to get
	Output: A set of questions
*/
unordered_set<string> MongoDbDatabase::getQuestions(const int amount)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];
	unordered_set<string> answer;

	int curr_amount = amount, trys = DBUSE_EXECUTING_TRYS;

	for (int i = 0, curr_amount = amount; curr_amount > 0 && i < DBUSE_EXECUTING_TRYS; i++, curr_amount = amount - answer.size())
	{
		// Aggregation for randomiazation -> sample enables it
		mongocxx::pipeline p{};
		p.sample(amount);
		p.project(make_document(kvp(DBUSE_QUESTIONS_QUESTION, 1), kvp(DBUSE_MONGODB_ID, 0)));

		auto result = col.aggregate(p, mongocxx::options::aggregate{});

		// Normal find in the need of normality
		/*options::find ops;
		ops.limit(amount);
		ops.projection(make_document(kvp(DBUSE_QUESTION_QUESTION, 1), kvp(DBUSE_MONGODB_ID, 0)));
		auto result = col.find(make_document(), ops);*/


		for (auto&& doc : result)
		{
			bsoncxx::document::element docing = doc[DBUSE_QUESTIONS_QUESTION];
			if (docing)
			{
				//bsoncxx::string::to_string(docing.get_utf8().value);
				answer.insert(docing.get_utf8().value.to_string());
			}
		}
	}

	return answer;
}

/*
	Gets an average answer time of a player
	Input: The username
	Output: The average amount of time
*/
double MongoDbDatabase::getPlayerAverageAnswerTime(const string& username)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	// Normal find in the need of normality
	options::find ops;
	ops.projection(make_document(kvp(DBUSE_STATISTICS_TBL_NAME, 1), kvp(DBUSE_MONGODB_ID, 0), kvp("$in", DBUSE_STATISTICS_AVG_TIME_PER_QSTION)));
	auto result = col.find_one(make_document(kvp(DBUSE_USERNAME_TXT, username)), ops);

	if (result)
	{
		auto value = (*result).view()[DBUSE_STATISTICS_TBL_NAME][DBUSE_STATISTICS_AVG_TIME_PER_QSTION];

		if (value && value.type() == type::k_double) { return value.get_double(); }
	}

	return -1;
}

/*
	Gets the number of correct answers of a player
	Input: The username
	Output: The number of correct answers
*/
int MongoDbDatabase::getNumOfCorrectAnswers(const string& username)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	// Normal find in the need of normality
	options::find ops;
	ops.projection(make_document(kvp(DBUSE_STATISTICS_TBL_NAME, 1), kvp(DBUSE_MONGODB_ID, 0), kvp("$in", DBUSE_STATISTICS_CRCT_ANS)));
	auto result = col.find_one(make_document(kvp(DBUSE_USERNAME_TXT, username)), ops);

	if (result)
	{
		auto value = (*result).view()[DBUSE_STATISTICS_TBL_NAME][DBUSE_STATISTICS_CRCT_ANS];

		if (value && value.type() == type::k_double) { return value.get_double(); }
	}

	return -1;
}

/*
	Gets the total questions answered of a player
	Input: The username
	Output: The total question answered
*/
int MongoDbDatabase::getNumOfTotalAnswers(const string& username)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	// Normal find in the need of normality
	options::find ops;
	ops.projection(make_document(kvp(DBUSE_STATISTICS_TBL_NAME, 1), kvp(DBUSE_MONGODB_ID, 0), kvp("$in", make_array(DBUSE_STATISTICS_CRCT_ANS, DBUSE_STATISTICS_WRONG_ANS))));
	auto result = col.find_one(make_document(kvp(DBUSE_USERNAME_TXT, username)), ops);

	if (result)
	{
		auto value_crct = (*result).view()[DBUSE_STATISTICS_TBL_NAME][DBUSE_STATISTICS_CRCT_ANS];
		auto value_wrong = (*result).view()[DBUSE_STATISTICS_TBL_NAME][DBUSE_STATISTICS_WRONG_ANS];

		if (value_crct && value_wrong && value_crct.type() == type::k_int32 && value_wrong.type() == type::k_int32) { return value_crct.get_int32() + value_wrong.get_int32(); }
	}

	return -1;
}

/*
	Gets the number of games a player has played
	Input: The username
	Output: The number of games
*/
int MongoDbDatabase::getNumOfPlayerGames(const string& username)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	// Normal find in the need of normality
	options::find ops;
	ops.projection(make_document(kvp(DBUSE_STATISTICS_TBL_NAME, 1), kvp(DBUSE_MONGODB_ID, 0), kvp("$in", DBUSE_STATISTICS_NUM_GAMES)));
	auto result = col.find_one(make_document(kvp(DBUSE_USERNAME_TXT, username)), ops);

	if (result)
	{
		auto value = (*result).view()[DBUSE_STATISTICS_TBL_NAME][DBUSE_STATISTICS_NUM_GAMES];

		if (value && value.type() == type::k_double) { return value.get_double(); }
	}

	return -1;
}

/*
	The function gets the statistics of the top number of users.
	Input: limit = The limit of the top users extraction number - const int;
	Output: The statistics of the number of the top users - json;
*/
json MongoDbDatabase::getTopUserStatistics(const int limit)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];
	json answer = {};

	// Normal find in the need of normality
	mongocxx::pipeline p{};
	string variables("$");
	variables += DBUSE_STATISTICS_TBL_NAME;
	variables += ".";
	p.add_fields(make_document(kvp(DBUSE_STATISTICS_RANKING, make_document(kvp("$let",
		make_document(kvp("vars", make_document(
			kvp("multi", make_document(kvp("$multiply", make_array((variables + DBUSE_STATISTICS_SCORE_POINTS), (variables + DBUSE_STATISTICS_CRCT_ANS))))),
			kvp("divi", make_document(kvp("$add", make_array((variables + DBUSE_STATISTICS_WRONG_ANS), 1)))))),
			kvp("in", make_document(kvp("$divide", make_array("$$multi", "$$divi"))))))))));
	p.sort(make_document(kvp(DBUSE_STATISTICS_RANKING, -1)));
	p.project(make_document(kvp(DBUSE_USERNAME_TXT, 1), kvp(DBUSE_STATISTICS_RANKING, 1), kvp(DBUSE_MONGODB_ID, 0)));
	p.limit(limit);

	auto result = col.aggregate(p, mongocxx::options::aggregate{});

	int num = 1;
	for (auto&& doc : result)
	{
		answer[std::to_string(num)] = answer.parse(bsoncxx::to_json(doc));
		num++;
	}

	return answer;
}

/*
	The function gets the statistics of a specified user.
	Input: username = The username to get the statistics off - const string&;
	Output: The statistics about the user - json;
*/
json MongoDbDatabase::getUserStatistics(const string& username)
{
	auto client = this->_clientsPool.acquire();
	collection col = (*client)[DBUSE_MONGODB_NAME][DBUSE_COLLECTION_NAME];

	// Normal find in the need of normality
	options::find ops;
	ops.projection(make_document(kvp(DBUSE_STATISTICS_TBL_NAME, 1), kvp(DBUSE_MONGODB_ID, 0), kvp("$in", DBUSE_STATISTICS_CRCT_ANS)));
	auto result = col.find_one(make_document(kvp(DBUSE_USERNAME_TXT, username)), ops);

	if (result)
	{
		auto value_stats = (*result).view()[DBUSE_STATISTICS_TBL_NAME];
		if (value_stats && value_stats.type() == type::k_document)
		{
			return json::parse(bsoncxx::to_json(value_stats.get_document()));
		}
	}

	return json();
}

typeDBClasses MongoDbDatabase::getClassType() const
{
	return MongoDbDatabase::classType;
}

/*
	The function creates the client of the thread that calls the function.
	Input: None;
	Output: None;
*/
//void MongoDbDatabase::addClient()
//{
//	this->_acquireDatabase(DBUSE_MONGODB_NAME);
//}

/*
	The function erases the client of the thread that calls the function.
	Input: None;
	Output: If the client was found and erased - bool;
*/
//bool MongoDbDatabase::deleteClient()
//{
//	thread::id threadsId = std::this_thread::get_id();
//	auto client =  this->_clients.find(threadsId);
//
//	if (client != this->_clients.end())
//	{
//		this->_clients.erase(client);
//		return true;
//	}
//
//	return false;
//}

//pool& MongoDbDatabase::getClientsPool()
//{
//	return this->_clientsPool;
//}
 

// Client entry functions -> Client entry is a holder for an object of the given pool!
//clientEntry::clientEntry() : _poolEntry(nullptr), _database(nullptr) {}
//clientEntry::clientEntry(pool& clientPool) : _poolEntry(nullptr), _database(nullptr) { _poolEntry = new pool::entry(clientPool.acquire()); }
//clientEntry::clientEntry(clientEntry&& other) noexcept { this->_poolEntry = other._poolEntry; this->_database = other._database; other._poolEntry = nullptr; other._database = nullptr; }
//clientEntry::clientEntry(clientEntry& other) noexcept { this->_poolEntry = other._poolEntry; this->_database = other._database; other._poolEntry = nullptr; other._database = nullptr; }
//
//clientEntry::clientEntry(pool& clientPool, const string& db_name) : _poolEntry(nullptr), _database(nullptr) { _poolEntry = new pool::entry(clientPool.acquire()); _database = new database((*(*this->_poolEntry))[db_name]); }
//
//clientEntry& clientEntry::operator=(clientEntry& other) noexcept { this->_poolEntry = other._poolEntry; this->_database = other._database; other._poolEntry = nullptr; other._database = nullptr; return (*this); }
//clientEntry& clientEntry::operator=(clientEntry&& other) noexcept { this->_poolEntry = other._poolEntry; this->_database = other._database; other._poolEntry = nullptr; other._database = nullptr; return (*this); }
//
//clientEntry::~clientEntry() { if (_poolEntry != nullptr) { delete _poolEntry; _poolEntry = nullptr; } if (_database != nullptr) { delete _database; _database = nullptr; } }

/*
	The function aquires set the new client by getting it out of the client pool.
	Input: clientPool = The pool to get out the clients for the current thread's - pool&;
	Output: The client that was created - client&;
*/
//client& clientEntry::setClient(pool& clientPool)
//{
//	if (_poolEntry == nullptr) { _poolEntry = new pool::entry(clientPool.acquire()); }
//	else { delete _poolEntry; _poolEntry = nullptr; _poolEntry = new pool::entry(clientPool.acquire()); }
//	return (*(*_poolEntry));
//}

/*
	The function gets the already existsing client.
	Input: None;
	Output: The wanted client - client&;
*/
//client& clientEntry::getClient() const
//{
//	if (_poolEntry != nullptr)
//	{
//		return (*(*_poolEntry));
//	}
//
//	throw std::runtime_error(__FUNCTION__" -> Invalid request for a client!");
//}

/*
	The function sets the new database by the given name out of the already existing client.
	Input: db_name = The name of the new dataabse to open - const string&;
	Output: The new database - database&;
*/
//database& clientEntry::setDatabase(const string& db_name)
//{
//	if (this->_poolEntry != nullptr)
//	{
//		if (this->_database == nullptr)
//		{
//			this->_database = new database((*(*this->_poolEntry))[db_name]);
//		}
//		else
//		{
//			delete this->_database;
//			this->_database = nullptr;
//			this->_database = new database((*(*this->_poolEntry))[db_name]);
//		}
//
//		return (*(this->_database));
//	}
//
//	throw std::runtime_error(__FUNCTION__" -> Invalid database creation! The entry wasn't yet created!");
//}

/*
	The function gets the already existing database.
	Input: None;
	Output: The wanted database - database&;
*/
//database& clientEntry::getDatabase()
//{
//	if (this->_database != nullptr)
//	{
//		return (*(this->_database));
//	}
//
//	throw std::runtime_error(__FUNCTION__" -> Invalid database creation! The entry wasn't yet created!");
//}

/*
	The function checks if the given clientEntry has the database already defined.
	Input: None;
	Output: If the database already exists and defined - bool;
*/
//bool clientEntry::hasDatabase() const noexcept
//{
//	return this->_database != nullptr;
//}
