#pragma once

// Constants Defines
#define DBUSE

#include <sstream>
#include <iostream>
#include <io.h>
#include <mutex>
#include <unordered_set>
#include "IDatabase.h"
#include "SingletonTemplate.h"
#include "Constants.h"
#include "sqlite3.h"
#include "json.hpp"

using std::stringstream;
using std::mutex;
using std::lock_guard;
using std::pair;

using namespace nlohmann;

class SqliteDatabase : public SingletonTemplate<SqliteDatabase, IDatabase>
{
private:
	// Variables
	sqlite3* _db;
	mutex _queryMutex;

	// Functions & Methods
	void _execute_db(const string& command, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data);
	void _execute_db_tansaction(const string& command, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data);

	// Constructors
	SqliteDatabase();

	// Class Workout
	friend class SingletonTemplate;

public:
	// Variables
	static const typeDBClasses classType;

	// Constructors & Destructors
	virtual ~SqliteDatabase();

	// Functions & Methods
	virtual bool doesUserExist(const string& username) override;
	virtual bool doesUserExist(const string& username, const string& password) override;
	virtual bool doesPasswordMatch(const string& password) override;
	virtual void addNewUser(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate) override;
	virtual unordered_set<string> getQuestions(const int amount);
	virtual double getPlayerAverageAnswerTime(const string& username);
	virtual int getNumOfCorrectAnswers(const string& username);
	virtual int getNumOfTotalAnswers(const string& username);
	virtual int getNumOfPlayerGames(const string& username);
	virtual json getTopUserStatistics(const int limit);
	virtual json getUserStatistics(const string& username);
	virtual typeDBClasses getClassType() const;
};

