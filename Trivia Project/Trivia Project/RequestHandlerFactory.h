#pragma once

#include "LoginManager.h"
#include "IDatabase.h"
#include "MongoDbDatabase.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "SingletonTemplate.h"
#include "Constants.h"

class LoginRequestHandler;
class MenuRequestHandler;

class RequestHandlerFactory : public SingletonTemplate<RequestHandlerFactory>
{
private:
	// Variables
	IDatabase& _database;
	LoginManager& _loginManager;
	RoomManager& _roomManager;
	StatisticsManager& _statisticsManager;

	// Constructors
	RequestHandlerFactory(IDatabase& database);

	// Class Workout
	friend class SingletonTemplate;

public:
	// Constructors & Destructors
	virtual ~RequestHandlerFactory();
	
	// Functions & Methods
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(const string& username);
	LoginManager& getLoginManager();
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	//bool initializeClientDB();
	//bool destroyClientDB();
};

