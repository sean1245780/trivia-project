#pragma once

#include <functional>
#include <utility>

template<class subclass, class... superclasses>
class SingletonTemplate : public superclasses...
{
protected:
    // Constructors & Destructors
    SingletonTemplate() = default;
    SingletonTemplate(const SingletonTemplate&) = delete;
    virtual ~SingletonTemplate() = default;

public:
    SingletonTemplate& operator=(SingletonTemplate & singletonTemplate) // Operator overloading
    {
        return singletonTemplate;
    }

    template<class... Args>
    static subclass& getInstance(Args... arguments) // Getting instance
    {
        static subclass singleton{ std::forward<Args>(arguments)... };

        return singleton;
    }

};