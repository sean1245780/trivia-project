#pragma once

#include <iostream>
#include <string>
#include <mutex>
#include <streambuf>
#include <ios>

namespace ostreamts
{
	using std::cout;
	using std::ostream;
	using std::basic_ostream;
	using std::mutex;
	using std::lock_guard;
	using std::streambuf;
	using std::ios;
	using std::ios_base;

	// This class is a thread safe use of the ostream -> by useing cout and a mutex.
	class ostreamTS
	{
	protected:
		static mutex _cmdOutputMutex;

	public:
		friend ostreamTS& operator<< (ostreamTS& output, bool val);
		friend ostreamTS& operator<< (ostreamTS& output, short val);
		friend ostreamTS& operator<< (ostreamTS& output, unsigned short val);
		friend ostreamTS& operator<< (ostreamTS& output, int val);
		friend ostreamTS& operator<< (ostreamTS& output, unsigned int val);
		friend ostreamTS& operator<< (ostreamTS& output, long val);
		friend ostreamTS& operator<< (ostreamTS& output, unsigned long val);
		friend ostreamTS& operator<< (ostreamTS& output, long long val);
		friend ostreamTS& operator<< (ostreamTS& output, unsigned long long val);
		friend ostreamTS& operator<< (ostreamTS& output, float val);
		friend ostreamTS& operator<< (ostreamTS& output, double val);
		friend ostreamTS& operator<< (ostreamTS& output, long double val);
		friend ostreamTS& operator<< (ostreamTS& output, void* val);
		friend ostreamTS& operator<< (ostreamTS& output, streambuf* val);
		friend ostreamTS& operator<< (ostreamTS& output, ostream& (*val)(ostream&));
		friend ostreamTS& operator<< (ostreamTS& output, ostreamTS& (*val)(ostreamTS&));
		friend ostreamTS& operator<< (ostreamTS& output, ios& (*val)(ios&));
		friend ostreamTS& operator<< (ostreamTS& output, ios_base& (*val)(ios_base&));
		friend ostreamTS& operator<< (ostreamTS& output, char val);
		friend ostreamTS& operator<< (ostreamTS& output, signed char val);
		friend ostreamTS& operator<< (ostreamTS& output, unsigned char val);
		friend ostreamTS& operator<< (ostreamTS& output, const char* val);
		friend ostreamTS& operator<< (ostreamTS& output, const signed char* val);
		friend ostreamTS& operator<< (ostreamTS& output, const unsigned char* val);

		template<class T>
		friend ostreamTS& operator<< (ostreamTS&& output, const T& val)
		{
			lock_guard locking(ostreamTS::_cmdOutputMutex);
			cout << val;
			return output;
		}
	};

	static ostreamTS coutts;

	// Can be used to rewrite the class - Need to inherit.
	/*
	#ifdef _M_CEE_PURE
	__PURE_APPDOMAIN_GLOBAL extern ostreamTS coutts, * _Ptr_coutts;
	#else // _M_CEE_PURE
	__PURE_APPDOMAIN_GLOBAL extern _CRTDATA2_IMPORT ostreamTS coutts, * _Ptr_coutts;
	#endif
	*/
}