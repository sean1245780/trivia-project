#pragma once

#include <exception>
#include <string>
#include "Constants.h"

class AnswerException : public std::exception
{
protected:
	// Variables
	std::string _messgae;
	unsigned_byte _status;

public:
	// Constructors & Destructors
	AnswerException(const unsigned_byte status, const std::string& message) : _messgae(message), _status(status) {}
	virtual ~AnswerException() noexcept = default;

	// Methods & Functions
	virtual const char* what() const noexcept { return _messgae.c_str(); }
	virtual const unsigned_byte getStatus() const noexcept { return this->_status; }
};