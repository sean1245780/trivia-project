#include "SqliteDatabase.h"

const typeDBClasses SqliteDatabase::classType = ID_SQLITE_DB;

/*
	The function executes a basic query in the sqlite3 database.
	Input: command = The command to execute - const string; callback = The function to execute by the output off the database - int (*)(void*, int, char**, charr**);
			data = The variable to pass into the callback function - void*;
	Output: None;
*/
void SqliteDatabase::_execute_db(const string& command, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data)
{
	char* error_exc_msg = nullptr;
	int result = 0;

	{ 
		lock_guard<mutex> locking(this->_queryMutex);
		result = sqlite3_exec(this->_db, command.c_str(), callback, data, &error_exc_msg);
	}

	if (result != SQLITE_OK)
	{
		stringstream error_msg;
		error_msg << "SQL database error (Code: " << result << ") -> " << error_exc_msg;
		delete[] error_exc_msg;
		error_exc_msg = nullptr;
		throw std::exception(error_msg.str().c_str());
	}
}

/*
	The function enables multiple queries in one execution through transaction in the sqlite3 database.
	Input: command = The command to execute - const string&; callback = The function to execute by the output off the database - int (*)(void*, int, char**, charr**);
			data = The variable to pass into the callback function - void*;
	Output: None;
*/
void SqliteDatabase::_execute_db_tansaction(const string& command, int(*callback)(void* data, int argc, char** argv, char** azColName), void* data)
{
	char* error_exc_msg = nullptr;
	string n_command = "begin;";
	n_command += command;
	int result = 0;

	{ 
		lock_guard<mutex> locking(this->_queryMutex);
		result = sqlite3_exec(this->_db, n_command.c_str(), callback, data, &error_exc_msg);
	}

	if (result != SQLITE_OK) // If didn't work needed to close the transaction
	{
		stringstream error_msg;
		error_msg << "SQL database error (Code: " << result << ") -> " << error_exc_msg;
		delete[] error_exc_msg;
		error_exc_msg = nullptr;

		n_command = "rollback;";
		int err_result = 0;

		{ 
			lock_guard<mutex> locking(this->_queryMutex);
			err_result = sqlite3_exec(this->_db, n_command.c_str(), nullptr, nullptr, &error_exc_msg);
		}

		if (err_result != SQLITE_OK)
		{
			error_msg << "\t+\tSQL database error (Code: " << err_result << "): Rollback -> " << error_exc_msg;
			delete[] error_exc_msg;
			error_exc_msg = nullptr;
		}

		throw std::exception(error_msg.str().c_str());
	}
	else
	{
		n_command = "commit;";
		
		{ 
			lock_guard<mutex> locking(this->_queryMutex);
			result = sqlite3_exec(this->_db, n_command.c_str(), nullptr, nullptr, &error_exc_msg);
		}

		if (result != SQLITE_OK)
		{
			stringstream error_msg;
			error_msg << "SQL database error (Code: " << result << "): Commit -> " << error_exc_msg;
			delete[] error_exc_msg;
			error_exc_msg = nullptr;
			throw std::exception(error_msg.str().c_str());
		}
	}
}

/*
	The function intializes the SqliteDatabase class.
	Input: None;
	Output: None;
*/
SqliteDatabase::SqliteDatabase()
{
	int exists = _access(DBUSE_DB_NAME, 0);
	int res = sqlite3_open(DBUSE_DB_NAME, &(this->_db)); // Opens or creates the database
	if (res != SQLITE_OK)
	{
		_db = nullptr;
		throw std::exception(string("Error occurred in the database opening! -> Code: " + std::to_string(res)).c_str());
	}

	if (exists == -1) // 0 for exists and -1 for none existance of the database
	{
		stringstream query;
		query << "create table " << DBUSE_COLLECTION_NAME << " (id integer primary key AUTOINCREMENT, " << DBUSE_USERNAME_TXT << " text unique not null, " << DBUSE_PASSWORD_TXT << " text not null, " << DBUSE_EMAIL_TXT << " text not null, " <<
			DBUSE_ADDRESS_TXT << " text not null, " << DBUSE_PHONE_TXT << " text not null, " << DBUSE_BIRTH_DATE_TXT << " text not null);";

		query << "create table " << DBUSE_QUESTIONS_TBL_NAME << " (id integer primary key AUTOINCREMENT, " << DBUSE_QUESTIONS_CATEGRY << " text not null, " << DBUSE_QUESTIONS_TYPE << " text not null, " << DBUSE_QUESTIONS_DIFFCLTY << " text not null, " <<
			DBUSE_QUESTIONS_QUESTION << " text unique not null, " << DBUSE_QUESTIONS_CRRCT_ANS << " text not null, " << DBUSE_QUESTIONS_INNCRCT_ANS << " text not null);";

		query << "create table " << DBUSE_STATISTICS_TBL_NAME << " (id integer primary key AUTOINCREMENT, " << DBUSE_STATISTICS_USER_ID << " integer not null, " << DBUSE_STATISTICS_NUM_GAMES << " integer not null DEFAULT 0, " << DBUSE_STATISTICS_CRCT_ANS << " integer not null DEFAULT 0, " <<
			DBUSE_STATISTICS_WRONG_ANS << " integer not null DEFAULT 0, " << DBUSE_STATISTICS_AVG_TIME_PER_QSTION << " real not null DEFAULT 0, " << DBUSE_STATISTICS_SCORE_POINTS << " integer not null DEFAULT 0, foreign key(" << DBUSE_STATISTICS_USER_ID << ") references " << DBUSE_COLLECTION_NAME << "(id));";

		_execute_db_tansaction(query.str(), nullptr, nullptr);
	}
}

/*
	The function destroys the SqliteDatabase class.
	Input: None;
	Output: None;
*/
SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

/*
	The function finds out if the user exists in the database.
	Input: username = User's username to look by - const string&;
	Output: If user by this username exists - bool;
*/
bool SqliteDatabase::doesUserExist(const string& username)
{
	stringstream query;
	query << "select count(*) from " << DBUSE_COLLECTION_NAME << " where " << DBUSE_USERNAME_TXT << " = '" << username << "';";

	int num = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			(*(int*)data) = atoi(argv[0]);
		}
		return 0;
	};

	_execute_db(query.str(), func, &num);

	return num > 0;
}

/*
	The function finds out if the user exists in the database.
	Input: username = User's username to look by - const string&; password = User's password to look by - const string&;
	Output: If user by this username and password exists - bool;
*/
bool SqliteDatabase::doesUserExist(const string& username, const string& password)
{
	stringstream query;
	query << "select count(*) from " << DBUSE_COLLECTION_NAME << " where " << DBUSE_USERNAME_TXT << " = '" << username << "' and " << DBUSE_PASSWORD_TXT << " = '" << password << "';";

	int num = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			(*(int*)data) = atoi(argv[0]);
		}
		return 0;
	};

	_execute_db(query.str(), func, &num);

	return num > 0;
}

/*
	The function finds out if there is user with the given password.
	Input: password = User's password to look by - const string&;
	Output: If user by this password exists - bool;
*/
bool SqliteDatabase::doesPasswordMatch(const string& password)
{
	stringstream query;
	query << "select count(*) from " << DBUSE_COLLECTION_NAME << " where " << DBUSE_PASSWORD_TXT << " = '" << password << "';";

	int num = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			(*(int*)data) = atoi(argv[0]);
		}
		return 0;
	};

	_execute_db(query.str(), func, &num);

	return num > 0;
}

/*
	The function adds new user to the data base.
	Input: username = User's username to add - const string&; password = User's password to add - const string&; email = User's email to add - const string&;
	Output: None;
*/
void SqliteDatabase::addNewUser(const string& username, const string& password, const string& email, const string& address, const string& phone, const string& birthdate)
{
	stringstream query;
	query << "insert into " << DBUSE_COLLECTION_NAME << " (" << DBUSE_USERNAME_TXT << ", " << DBUSE_PASSWORD_TXT << ", " << DBUSE_EMAIL_TXT << ", " << DBUSE_ADDRESS_TXT << ", " << DBUSE_PHONE_TXT << ", " <<
		DBUSE_BIRTH_DATE_TXT << ")  values ('" << username << "', '" << password << "', '" << email << "', '" << address << "', '" << phone << "', '" << birthdate << "');";
	std::cout << query.str() << "\n\n" << std::endl;
	_execute_db(query.str(), nullptr, nullptr);
	query.str("");

	query << "insert into " << DBUSE_STATISTICS_TBL_NAME << " (" << DBUSE_STATISTICS_USER_ID << ") values((select id from " << DBUSE_COLLECTION_NAME << " where " << DBUSE_USERNAME_TXT << " = '" << username << "' limit 1));";

	std::cout << query.str() << std::endl;

	_execute_db(query.str(), nullptr, nullptr);
}

/*
	Gets a specific amount of questions from the database
	Input: The amount of question to get
	Output: A set of questions
*/
unordered_set<string> SqliteDatabase::getQuestions(const int amount)
{
	stringstream query;
	query << "select " << DBUSE_QUESTIONS_QUESTION << " from " << DBUSE_QUESTIONS_TBL_NAME << " ORDER BY RANDOM() LIMIT " << amount << ";";

	unordered_set<string> questions;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			if (azColName[0] == std::string(DBUSE_QUESTIONS_QUESTION))
			{
				((unordered_set<string>*)data)->insert(argv[0]);
			}
		}

		return 0;
	};

	_execute_db(query.str(), func, &questions);

	return questions;
}

/*
	Gets an average answer time of a player
	Input: The username
	Output: The average amount of time
*/
double SqliteDatabase::getPlayerAverageAnswerTime(const string& username)
{
	stringstream query;
	query << "select " << DBUSE_STATISTICS_AVG_TIME_PER_QSTION << " from " << DBUSE_STATISTICS_TBL_NAME << " join " << DBUSE_COLLECTION_NAME << " on "
		<< DBUSE_STATISTICS_TBL_NAME << "." << DBUSE_STATISTICS_USER_ID << " = " << DBUSE_COLLECTION_NAME << ".id where " << DBUSE_COLLECTION_NAME << "." << DBUSE_USERNAME_TXT << " = '" << username << "';";

	double ans = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			if (azColName[0] == std::string(DBUSE_STATISTICS_AVG_TIME_PER_QSTION))
			{
				(*((double*)data)) = std::stod(argv[0]);
			}
		}

		return 0;
	};

	_execute_db(query.str(), func, &ans);

	return ans;
}

/*
	Gets the number of correct answers of a player
	Input: The username
	Output: The number of correct answers
*/
int SqliteDatabase::getNumOfCorrectAnswers(const string& username)
{
	stringstream query;
	query << "select " << DBUSE_STATISTICS_CRCT_ANS << " from " << DBUSE_STATISTICS_TBL_NAME << " join " << DBUSE_COLLECTION_NAME << " on "
		<< DBUSE_STATISTICS_TBL_NAME << "." << DBUSE_STATISTICS_USER_ID << " = " << DBUSE_COLLECTION_NAME << ".id where " << DBUSE_COLLECTION_NAME << "." << DBUSE_USERNAME_TXT << " = '" << username << "';";

	int ans = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			if (azColName[0] == std::string(DBUSE_STATISTICS_CRCT_ANS))
			{
				(*((int*)data)) = std::atoi(argv[0]);
			}
		}

		return 0;
	};

	_execute_db(query.str(), func, &ans);

	return ans;
}

/*
	Gets the total questions answered of a player
	Input: The username
	Output: The total question answered
*/
int SqliteDatabase::getNumOfTotalAnswers(const string& username)
{
	stringstream query;
	query << "select " << DBUSE_STATISTICS_CRCT_ANS << ", " << DBUSE_STATISTICS_WRONG_ANS << " from " << DBUSE_STATISTICS_TBL_NAME << " join " << DBUSE_COLLECTION_NAME << " on "
		<< DBUSE_STATISTICS_TBL_NAME << "." << DBUSE_STATISTICS_USER_ID << " = " << DBUSE_COLLECTION_NAME << ".id where " << DBUSE_COLLECTION_NAME << "." << DBUSE_USERNAME_TXT << " = '" << username << "';";

	int ans = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 2)
		{
			for (int i = 0; i < argc; i++)
			{
				if (azColName[i] == std::string(DBUSE_STATISTICS_CRCT_ANS) || azColName[i] == std::string(DBUSE_STATISTICS_WRONG_ANS))
				{
					(*((int*)data)) += std::atoi(argv[0]);
				}
			}
		}

		return 0;
	};

	_execute_db(query.str(), func, &ans);

	return ans;
}

/*
	Gets the number of games a player has played
	Input: The username
	Output: The number of games
*/
int SqliteDatabase::getNumOfPlayerGames(const string& username)
{
	stringstream query;
	query << "select " << DBUSE_STATISTICS_NUM_GAMES << " from " << DBUSE_STATISTICS_TBL_NAME << " join " << DBUSE_COLLECTION_NAME << " on "
		<< DBUSE_STATISTICS_TBL_NAME << "." << DBUSE_STATISTICS_USER_ID << " = " << DBUSE_COLLECTION_NAME << ".id where " << DBUSE_COLLECTION_NAME << "." << DBUSE_USERNAME_TXT << " = '" << username << "';";

	int ans = 0;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 1)
		{
			if (azColName[0] == std::string(DBUSE_STATISTICS_NUM_GAMES))
			{
				(*((int*)data)) = std::atoi(argv[0]);
			}
		}

		return 0;
	};

	_execute_db(query.str(), func, &ans);

	return ans;
}

/*
	The function gets the statistics of the top number of users.
	Input: limit = The limit of the top users extraction number - const int;
	Output: The statistics of the number of the top users - json;
*/
json SqliteDatabase::getTopUserStatistics(const int limit)
{
	stringstream query;
	string statsP = DBUSE_STATISTICS_TBL_NAME, usersP = DBUSE_COLLECTION_NAME;
	statsP += ".";
	usersP += ".";
	query << "select " << usersP << DBUSE_USERNAME_TXT << ", " << statsP << DBUSE_STATISTICS_CRCT_ANS << ", " << statsP << DBUSE_STATISTICS_WRONG_ANS << ", " << statsP << DBUSE_STATISTICS_SCORE_POINTS << " from "
		<< DBUSE_STATISTICS_TBL_NAME << " join " << DBUSE_COLLECTION_NAME << " on " << usersP << "id = " << statsP << DBUSE_STATISTICS_USER_ID << " order by " << DBUSE_STATISTICS_RANKING_CALCULATION << " desc limit " << limit << ";";

	json ans = {};
	int counting = 1;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 4)
		{
			pair<int&, json&>* vals = ((pair<int&, json&>*)data);
			double multip = 1, diving = 0;

			for (int i = 0; i < argc; i++)
			{
				if (azColName[i] == std::string(DBUSE_USERNAME_TXT))
				{
					vals->second[std::to_string(vals->first)][DBUSE_USERNAME_TXT] = argv[i];
				}
				else if (azColName[i] == std::string(DBUSE_STATISTICS_CRCT_ANS))
				{
					multip *= std::stod(argv[i]);
				}
				else if (azColName[i] == std::string(DBUSE_STATISTICS_WRONG_ANS))
				{
					diving = (std::stod(argv[i]) + 1);
				}
				else if (azColName[i] == std::string(DBUSE_STATISTICS_SCORE_POINTS))
				{
					multip *= std::stod(argv[i]);
				}
			}

			vals->second[std::to_string(vals->first)][DBUSE_STATISTICS_RANKING] = multip / diving;

			vals->first++;
		}

		return 0;
	};

	_execute_db(query.str(), func, &(pair<int&, json&>(counting, ans)));

	return ans;
}

/*
	The function gets the statistics of a specified user.
	Input: username = The username to get the statistics off - const string&;
	Output: The statistics about the user - json;
*/
json SqliteDatabase::getUserStatistics(const string& username)
{
	stringstream query;
	query << "select " << DBUSE_STATISTICS_NUM_GAMES << ", " << DBUSE_STATISTICS_CRCT_ANS << ", " << DBUSE_STATISTICS_WRONG_ANS << ", " << DBUSE_STATISTICS_AVG_TIME_PER_QSTION << ", " << DBUSE_STATISTICS_SCORE_POINTS << " from " << DBUSE_STATISTICS_TBL_NAME << " join " << DBUSE_COLLECTION_NAME << " on " <<
		DBUSE_STATISTICS_TBL_NAME << "." << DBUSE_STATISTICS_USER_ID << " = " << DBUSE_COLLECTION_NAME << ".id where " << DBUSE_COLLECTION_NAME << "." << DBUSE_USERNAME_TXT << " = '" << username << "' limit 1;";

	json ans;
	auto func = [](void* data, int argc, char** argv, char** azColName)
	{
		if (argc == 5)
		{
			for (int i = 0; i < argc; i++)
			{
				if (azColName[i] == std::string(DBUSE_STATISTICS_NUM_GAMES))
				{
					(*((json*)data))[DBUSE_STATISTICS_NUM_GAMES] = argv[i];
				}
				else if (azColName[i] == std::string(DBUSE_STATISTICS_CRCT_ANS))
				{
					(*((json*)data))[DBUSE_STATISTICS_CRCT_ANS] = argv[i];
				}
				else if (azColName[i] == std::string(DBUSE_STATISTICS_WRONG_ANS))
				{
					(*((json*)data))[DBUSE_STATISTICS_WRONG_ANS] = argv[i];
				}
				else if (azColName[i] == std::string(DBUSE_STATISTICS_SCORE_POINTS))
				{
					(*((json*)data))[DBUSE_STATISTICS_SCORE_POINTS] = argv[i];
				}
			}
		}

		return 0;
	};

	_execute_db(query.str(), func, &ans);

	return ans;
}

typeDBClasses SqliteDatabase::getClassType() const
{
	return SqliteDatabase::classType;
}
