#pragma once

	typedef char signed_byte;
	typedef unsigned char unsigned_byte;

#define SUCCESS 1
#define FAIL 0
#define STATUS_SIZE sizeof(unsigned_byte)
#define LENGTH_SIZE sizeof(unsigned int) 
#define RSSC_ERR_MSG_REQST_RELEVANT "Invalid request! States error occurred!" // Error messgae to be sent
#define MIN_YEAR 1900

	enum Point_Scores { Easy_Points = 1, Medium_Points = 3, Hard_Points = 5 };
	typedef enum { ID_SQLITE_DB = 0, ID_MONGODB_DB } typeDBClasses;
	typedef enum { RST_INIT_MODE = 0 } roomState;

	// Server constants
#ifdef CSRVR
#define CSRVR_EXIT "EXIT"
#endif // 


// JasonResponsePacketSerializer
#ifdef JRPS
#define JRPS_STATUS_TXT "status"
#define JRPS_MSG_TXT "message" 
#define JRPS_USERS_TXT "users"
#define JRPS_ID_TXT "roomId"
#define JRPS_NAME_TXT "roomName"
#define JRPS_MAX_PLAYERS_TXT "maxPlayers"
#define JRPS_NUM_PLAYERS_TXT "numPlayers"
#define JRPS_TIME_PER_QUESTION_TXT "timePerQuestion"
#define JRPS_QUESTION_COUNT_TXT "questionsCount"
#define JRPS_IS_ACTIVE_TXT "isActive"
#define JRPS_DATA_TXT "data"
#endif

// JasonResponsePacketDeserializer
#ifdef JRPD
#define JRPD_USERNAME_TXT "username"
#define JRPD_PASSWORD_TXT "password"
#define JRPD_EMAIL_TXT "email"
#define JRPD_ADDRESS_TXT "address"
#define JRPD_PHONE_TXT "phone_num"
#define JRPD_BIRTH_DATE_TXT "birth_date"
#define JRPD_ROOM_ID_TXT "roomId"
#define JRPD_ROOM_NAME_TXT "roomName"
#define JRPD_ANSWER_TIMEOUT_TXT "answerTimeout"
#define JRPD_MAX_USERS_TXT "maxUsers"
#define JRPD_QUESTION_COUNT_TXT "questionsCount"
#endif

#ifdef DBUSE
#define DBUSE_USERNAME_TXT "username"
#define DBUSE_PASSWORD_TXT "password"
#define DBUSE_EMAIL_TXT "email"
#define DBUSE_ADDRESS_TXT "address"
#define DBUSE_PHONE_TXT "phone_num"
#define DBUSE_BIRTH_DATE_TXT "birth_date"
#define DBUSE_DB_NAME "Trivia_DB.sqlite"
#define DBUSE_MONGODB_NAME "Trivia_DB"
#define DBUSE_COLLECTION_NAME "users"
#define DBUSE_INVALID_CHARS "/\\\"$*<>^&:|?!;-" // Removed '.' and added '-'
#define DBUSE_VALID_CHARS_PASSWORD "*&^%$#@!"
#define DBUSE_EMAIL_SIGN '@'
#define DBUSE_EMAIL_DOT '.'
// ----------------------------
#define DBUSE_QUESTIONS_TBL_NAME "questions"
#define DBUSE_QUESTIONS_CATEGRY "category"
#define DBUSE_QUESTIONS_TYPE "type"
#define DBUSE_QUESTIONS_DIFFCLTY "difficulty"
#define DBUSE_QUESTIONS_QUESTION "question"
#define DBUSE_QUESTIONS_CRRCT_ANS "correct_answer"
#define DBUSE_QUESTIONS_INNCRCT_ANS "incorrect_answers"
// ----------------------------
#define DBUSE_STATISTICS_TBL_NAME "statistics"
#define DBUSE_STATISTICS_USER_ID "user_id" // For sqlite
#define DBUSE_STATISTICS_NAME "username"
#define DBUSE_STATISTICS_NUM_GAMES "num_games"
#define DBUSE_STATISTICS_NUM_ANS "num_answers"
#define DBUSE_STATISTICS_CRCT_ANS "correct_answers"
#define DBUSE_STATISTICS_WRONG_ANS "wrong_answers"
#define DBUSE_STATISTICS_AVG_TIME_PER_QSTION "avg_time_per_question"
#define DBUSE_STATISTICS_SCORE_POINTS "score_points"
#define DBUSE_STATISTICS_RANKING "ranking"
// ----------------------------
#define DBUSE_MONGODB_DEFAULT_URI "mongodb://localhost:27017"
#define DBUSE_MONGODB_MIN_SIZE_POOL "minPoolSize=5"
#define DBUSE_MONGODB_MAX_SIZE_POOL "maxPoolSize=200"
#define DBUSE_MONGODB_ID "_id"
// ----------------------------
#define DBUSE_EXECUTING_TRYS 10
#define DBUSE_STATISTICS_RANKING_CALCULATION "(" << DBUSE_STATISTICS_SCORE_POINTS << " * " << DBUSE_STATISTICS_CRCT_ANS << ") / (" << DBUSE_STATISTICS_WRONG_ANS << " + 1.0)"
#endif

#ifdef STTSMNGR
#define STTSMNG_TOP_USERS_NUM 3
#define STTSMNG_MAX_TOP_USERS 50
#endif

#ifdef RMNGMNT
#define RMNGMNT_MIN_QUESTIONS 3
#define RMNGMNT_MAX_QUESTIONS 30
#define RMNGMNT_MIN_USERS 1
#define RMNGMNT_MAX_USERS 20
#define RMNGMNT_MIN_TIME 5
#define RMNGMNT_MAX_TIME 60
#define RMNGMNT_MIN_NAME_LEN 3
#define RMNGMNT_MAX_NAME_LEN 40
#endif


// Response Status Code (0 - 127)
#ifdef RSSC
	enum responseStatusCode 
	{
		RSSC_ERROR = 0, RSSC_LOGIN, RSSC_SIGNUP, RSSC_LOGOUT, RSSC_ILLGL_STR, RSSC_ILLGL_USERN, RSSC_ILLGL_PASSWRD,
		RSSC_ILLGL_EMAIL, RSSC_ILLGL_ADDRS, RSSC_ILLGL_PHONEN, RSSC_ILLGL_BRTHD_PRM, RSSC_WRONG_LOGIN_PRM, RSSC_ALRDY_TKN,
		RSSC_USER_ALRDY_CNNCTD, RSSC_GET_ROOMS, RSSC_GET_PLAYERS_IN_ROOM, RSSC_JOIN_ROOM, RSSC_GET_STATISTICS_USR,
		RSSC_GET_STATISTICS_TOP_USRS, RSSC_CREATE_ROOM, RSSC_ERR_USR_NOT_EXST, RSSC_ERR_ROOM_NOT_EXST
	};

// RSSC_GET_ROOMS -> Fail is just status 0;
// RSSC_GET_STATISTICS_TOP_USRS -> Fail is just status 0;
// RSSC_CREATE_ROOM -> Fail is just status 0;

#define RSSC_ILLGL_STR_TXT "Input contains illegal characters!"
#define RSSC_ILLGL_USERN_TXT "Illegal username was entered!"
#define RSSC_ILLGL_PASSWRD_TXT "Illegal password was entered!"
#define RSSC_ILLGL_EMAIL_TXT "Illegal email was entered!"
#define RSSC_ILLGL_ADDRS_TXT "Illegal address was entered!"
#define RSSC_ILLGL_PHONEN_TXT "Illegal phone number was entered!"
#define RSSC_ILLGL_BRTHD_PRM_TXT "Illegal birth date was entereds!"
#define RSSC_WRONG_LOGIN_PRM_TXT "Wrong username or password!"
#define RSSC_ALRDY_TKN_TXT "Username already taken!"
#define RSSC_USER_ALRDY_CNNCTD_TXT "The user already connected!"
#define RSSC_ERR_LOGOUT_TXT "Couldn't logout because user isn't connected!"
#define RSSC_ERR_GET_ROOMS_TXT "Couldn't get the active rooms!"
#define RSSC_ERR_GET_PLAYERS_IN_ROOM_TXT "Couldn't get players into the given room!"
#define RSSC_ERR_GET_STATISTICS_TXT "Couldn't get the statistics!"
#define RSSC_ERR_JOIN_ROOM_TXT "Couldn't join the given room!"
#define RSSC_ERR_CREATE_ROOM_TXT "Couldn't create the wanted room!"
#endif

	// Request Status Code (128 - 255)
#ifdef RQSC
	enum requestStatusCode { RQSC_LOGOUT = 128, RQSC_SIGNUP, RQSC_LOGIN, RQSC_CREATE_ROOM, RQSC_JOIN_ROOM, RQSC_GET_STATISTICS_USR, RQSC_GET_STATISTICS_TOP_USRS, RQSC_GET_PLAYERS_IN_ROOM, RQSC_GET_ROOMS };
#endif
