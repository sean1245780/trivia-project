#pragma once

#define DBUSE
#define STTSMNGR
#define RSSC

#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream> // DELETE!
#include "SingletonTemplate.h"
#include "IDatabase.h"
#include "AnswerException.h"
#include "Constants.h"

using std::string;
using std::unordered_map;
using std::stringstream;

typedef struct userStats
{
public:
	string username;
	unsigned int num_games;
	unsigned int num_answers;
	unsigned int correct_answers;
	unsigned int wrong_answers;
	double avg_time_per_question;
	unsigned int score_points;
	double ranking;

	userStats() : num_games(0), num_answers(0), correct_answers(0), wrong_answers(0),
		avg_time_per_question(0), score_points(0), ranking(0) {}
} userStats;


class StatisticsManager : public SingletonTemplate<StatisticsManager>
{
private:
	// Variables
	IDatabase& _database;

	//Constructor
	StatisticsManager(IDatabase& database) : _database(database) {}

	friend class SingletonTemplate;

public:
	virtual ~StatisticsManager();

	userStats getStatisticsStruct(const string& username);
	unordered_map<int, userStats> getStatisticsStruct(const int amount);
	json getStatisticsJson(const string& username);
	json getStatisticsJson(const int amount);

};

