#pragma once

#include <string>
#include <ctime>
#include "Constants.h"

using std::string;

typedef struct RequestResult;

typedef struct RequestInfo
{
public:
	unsigned_byte id;
	time_t recivalTime;
	string buffer; // Should be vector or array but lets be serious, string is the best

} RequestInfo;


class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo info) = 0;
	virtual RequestResult handleRequest(RequestInfo info) = 0;
	virtual bool disconnectUser() = 0;
};


typedef struct RequestResult
{
public:
	string response; // Should be vector or array but lets be serious, string is the best
	IRequestHandler* newHandler;

} RequestResult;