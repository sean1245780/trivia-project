#include "StatisticsManager.h"

/*
	Destructs a statistics manager
	Input: None
	Output: None
*/
StatisticsManager::~StatisticsManager()
{
}

/*
	The function gets the statistics of a given user.
	Input: username = The name of the user to look for - const string&;
	Output: The statistics of the user - userStats;
*/
userStats StatisticsManager::getStatisticsStruct(const string& username)
{
	json data = this->_database.getUserStatistics(username);
	userStats ans;

	try {
		ans.username = username;
		ans.num_games = data.at(DBUSE_STATISTICS_NUM_GAMES).get<unsigned int>();
		ans.correct_answers = data.at(DBUSE_STATISTICS_CRCT_ANS).get<unsigned int>();
		ans.wrong_answers = data.at(DBUSE_STATISTICS_WRONG_ANS).get<unsigned int>();
		ans.avg_time_per_question = data.at(DBUSE_STATISTICS_AVG_TIME_PER_QSTION).get<double>();
		ans.score_points = data.at(DBUSE_STATISTICS_SCORE_POINTS).get<unsigned int>();
		ans.num_answers = ans.correct_answers + ans.wrong_answers;
		ans.ranking = ((double)(ans.correct_answers * ans.score_points)) / (ans.wrong_answers + 1);
	}
	catch (const std::exception& e)
	{
		throw AnswerException(RSSC_ERR_USR_NOT_EXST, ("Statistics of the given user weren't given right! -> " + username));
	}

	return ans;
}

/*
	The function gets the statistics of a the top number of players.
	Input: None;
	Output: The top number of users - unordered_map<int, userStats>;
*/
unordered_map<int, userStats> StatisticsManager::getStatisticsStruct(const int amount)
{
	if (amount > STTSMNG_MAX_TOP_USERS) { throw std::exception((string("Invalid amount of users to get statistics of! -> ") += __FUNCTION__).c_str()); }
	
	json data = this->_database.getTopUserStatistics(amount);
	unordered_map<int, userStats> ans;

	try 
	{
		json inner;
		string index_str = "";
		bool stop = false; // For stoping in a situation of the database small num of users
		const int default_num = 1; // For checking unempty dictionary
		for (int i = default_num; i <= amount && !stop; i++)
		{
			userStats statsing;
			index_str = std::to_string(i);

			try
			{
				inner = data.at(index_str);
			}
			catch(const std::exception& e)
			{
				if (i > default_num) { stop = true; }
				else { throw  AnswerException(RSSC_GET_STATISTICS_TOP_USRS, ("Statistics of the top number of users weren't found! -> " + std::to_string(amount))); }
			}

			if (!stop)
			{
				statsing.username = inner.at(DBUSE_STATISTICS_NAME).get<std::string>();
				statsing.ranking = inner.at(DBUSE_STATISTICS_RANKING).get<double>();
				ans.insert(std::pair<int, userStats>(i, statsing));
			}
		}
	}
	catch (const std::exception& e)
	{
		throw AnswerException(RSSC_GET_STATISTICS_TOP_USRS, ("Statistics of the top number of users weren't found! -> " + std::to_string(amount)));
	}

	return ans;
}

/*
	The function gets the statistics of a given user.
	Input: username = The name of the user to look for - const string&;
	Output: The statistics of the user - json;
*/
json StatisticsManager::getStatisticsJson(const string& username)
{
	json answer = this->_database.getUserStatistics(username);

	if (answer.empty()) { throw AnswerException(RSSC_ERR_USR_NOT_EXST, ("Statistics of the given user weren't found! -> " + username)); }

	try
	{
		answer[DBUSE_STATISTICS_NUM_ANS] = ((answer.at(DBUSE_STATISTICS_CRCT_ANS).get<unsigned int>()) + (answer.at(DBUSE_STATISTICS_WRONG_ANS).get<unsigned int>()));
		answer[DBUSE_STATISTICS_RANKING] = (((double)(answer.at(DBUSE_STATISTICS_CRCT_ANS).get<unsigned int>()) * (answer.at(DBUSE_STATISTICS_SCORE_POINTS).get<unsigned int>())) /
											((answer.at(DBUSE_STATISTICS_WRONG_ANS).get<unsigned int>()) + 1));
	}
	catch (const std::exception& e)
	{
		throw AnswerException(RSSC_ERR_USR_NOT_EXST, ("Statistics of the given user weren't given right! -> " + username));
	}

	return answer;
}

/*
	The function gets the statistics of a the top number of players.
	Input: None;
	Output: The top number of users - json;
*/
json StatisticsManager::getStatisticsJson(const int amount)
{
	if (amount > STTSMNG_MAX_TOP_USERS) { throw std::exception((string("Invalid amount of users to get statistics of! -> ") += __FUNCTION__).c_str()); }

	const json& answer = this->_database.getTopUserStatistics(amount);

	if (answer.empty()) { throw AnswerException(RSSC_GET_STATISTICS_TOP_USRS, ("Statistics of the given top number of users weren't found! -> " + std::to_string(amount))); }

	return answer;
}