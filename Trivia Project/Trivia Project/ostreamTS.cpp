#include "ostreamTS.h"

namespace ostreamts
{
	mutex ostreamTS::_cmdOutputMutex;

	ostreamTS& operator<<(ostreamTS& output, bool val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%d", ((val) ? (1) : (0)));
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, short val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%hi", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, unsigned short val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%hu", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, int val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%d", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, unsigned int val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%u", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, long val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%li", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, unsigned long val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%lu", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, long long val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%lli", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, unsigned long long val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%llu", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, float val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%f", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, double val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%lf", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, long double val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%Lf", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, void* val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%p", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, streambuf* val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		cout << val;
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, ostream& (*val)(ostream&))
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		cout << val;
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, ostreamTS& (*val)(ostreamTS&))
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		cout << val;
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, ios& (*val)(ios&))
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		cout << val;
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, ios_base& (*val)(ios_base&))
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		cout << val;
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, char val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%c", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, signed char val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%c", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, unsigned char val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%c", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, const char* val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%s", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, const signed char* val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%s", val);
		return output;
	}

	ostreamTS& operator<<(ostreamTS& output, const unsigned char* val)
	{
		lock_guard<mutex> locking(ostreamTS::_cmdOutputMutex);
		printf("%s", val);
		return output;
	}
}