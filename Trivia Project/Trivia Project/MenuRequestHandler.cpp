#include "MenuRequestHandler.h"

/*
	Signs out of the server
	Input: The request info
	Output: The result of the request
*/
RequestResult MenuRequestHandler::signout(const RequestInfo& info)
{
	RequestResult result{ "", nullptr };
	LogoutResponse response { 0 };
	unsigned_byte status = 0;

	bool ans = this->_requestHandlerFactory.getLoginManager().signout(this->_loggedUser.getUsername());

	if (ans)
	{
		response.status = SUCCESS;
		result.newHandler = this->_requestHandlerFactory.createLoginRequestHandler();
		status = RSSC_LOGOUT;
	}
	else
	{
		response.status = FAIL;
		result.newHandler = this->_requestHandlerFactory.createLoginRequestHandler(); // Return to default state
		status = RSSC_ERR_USR_NOT_EXST;
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	gets the rooms available
	Input: The request info
	Output: The result of the request
*/
RequestResult MenuRequestHandler::getRooms(const RequestInfo& info)
{
	RequestResult result{ "", nullptr };
	GetRoomsResponse response;
	unsigned_byte status = 0;

	response.rooms = this->_requestHandlerFactory.getRoomManager().getRooms();
	
	if (!response.rooms.empty())
	{
		response.status = SUCCESS;
		status = RSSC_GET_ROOMS;
	}
	else
	{
		response.status = FAIL;
		status = RSSC_GET_ROOMS;
	}
	
	result.newHandler = nullptr;
	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	Gets all players in a room
	Input: The request info
	Output: The result of the request
*/
RequestResult MenuRequestHandler::getPlayersInRoom(const RequestInfo& info)
{
	RequestResult result { "", nullptr };
	GetPlayersInRoomRequest request = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(info.buffer);
	GetPlayersInRoomResponse response;
	unsigned_byte status = 0;
	
	try 
	{
		response.status = SUCCESS;
		response.players = this->_requestHandlerFactory.getRoomManager().getAllRooms().at(request.roomId).getAllUsers();
		status = RSSC_GET_PLAYERS_IN_ROOM;
	}
	catch (const std::exception& e)
	{
		response.status = FAIL;
		status = RSSC_ERR_ROOM_NOT_EXST;
	}

	result.newHandler = nullptr;
	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	Gets the statistics of the user.
	Input: info = The request info object with the request - const RequestInfo&;
	Output: The result of the request - RequestResult;
*/
RequestResult MenuRequestHandler::getStatisticsUsr(const RequestInfo& info)
{
	RequestResult result { "", nullptr };
	GetStatisticsResponse response;
	unsigned_byte status = 0;

	try
	{
		response.status = SUCCESS;
		response.statistics = this->_requestHandlerFactory.getStatisticsManager().getStatisticsJson(this->_loggedUser.getUsername());
		status = RSSC_GET_STATISTICS_USR;
	}
	catch (const AnswerException& e)
	{
		response.status = FAIL;
		status = RSSC_ERR_USR_NOT_EXST;
	}
	catch (const std::exception& e)
	{
		throw std::exception((string("Invalid extraction data of user's statistics! -> ") += e.what()).c_str());
	}

	result.newHandler = nullptr;
	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	Gets the statistics of the top number of users.
	Input: info = The request info object with the request - const RequestInfo&;
	Output: The result of the request - RequestResult;
*/
RequestResult MenuRequestHandler::getStatisticsTopUsrs(const RequestInfo& info)
{
	RequestResult result{ "", nullptr };
	GetStatisticsResponse response;
	unsigned_byte status = 0;

	try
	{
		response.status = SUCCESS;
		response.statistics = this->_requestHandlerFactory.getStatisticsManager().getStatisticsJson(STTSMNG_TOP_USERS_NUM);
		status = RSSC_GET_STATISTICS_TOP_USRS;
	}
	catch (const AnswerException& e)
	{
		response.status = FAIL;
		status = RSSC_GET_STATISTICS_TOP_USRS;
	}
	catch (const std::exception& e)
	{
		throw std::exception((string("Invalid extraction data of top amount users' statistics! -> ") += e.what()).c_str());
	}

	result.newHandler = nullptr;
	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	Joins a selected room
	Input: The request info
	Output: The result of the request
*/
RequestResult MenuRequestHandler::joinRoom(const RequestInfo& info)
{
	RequestResult result;
	JoinRoomRequest request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);
	JoinRoomResponse response;
	unsigned_byte status = 0;

	try
	{
		response.status = SUCCESS;
		this->_requestHandlerFactory.getRoomManager().getAllRooms().at(request.roomId).addUser(this->_loggedUser);
		result.newHandler = nullptr; // set to room admin handler
		status = RSSC_JOIN_ROOM;
	}
	catch (const std::exception& e)
	{
		response.status = FAIL;
		result.newHandler = nullptr;
		status = RSSC_ERR_ROOM_NOT_EXST;
	}
	
	result.response = JsonResponsePacketSerializer::serializeResponse(response, status);

	return result;
}

/*
	Creates a room
	Input: The request info
	Output: The result of the request
*/
RequestResult MenuRequestHandler::createRoom(const RequestInfo& info)
{
	RequestResult result;
	CreateRoomRequest request;
	CreateRoomResponse response;
	RoomData data;

	try
	{
		
		request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);

		data.id = this->_requestHandlerFactory.getRoomManager().getNextId();
		data.name = request.roomName;
		data.maxPlayers = request.maxUsers;
		data.numQuestions = request.questionCount;
		data.timePerQuestion = request.answerTimeout;
		data.isActive = RST_INIT_MODE;

		this->_requestHandlerFactory.getRoomManager().createRoom(this->_loggedUser, data);
		response.status = SUCCESS;
	}
	catch (const AnswerException& e)
	{
		response.status = FAIL;
	}
	

	result.newHandler = nullptr; // set to room admin handler
	result.response = JsonResponsePacketSerializer::serializeResponse(response, RSSC_CREATE_ROOM);

	return result;
}

/*
	Destructs the menu request handler
	Input: None
	Output: None 
*/
MenuRequestHandler::~MenuRequestHandler()
{
}

/*
	Checks if the request is relevant
	Input: The info of the request
	Output: Whether it is relevant or not
*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo info)
{
	return info.id == RQSC_LOGOUT || info.id == RQSC_CREATE_ROOM || info.id == RQSC_GET_PLAYERS_IN_ROOM
		|| info.id == RQSC_GET_ROOMS || info.id == RQSC_GET_STATISTICS_USR || info.id == RQSC_GET_STATISTICS_TOP_USRS ||
		info.id == RQSC_JOIN_ROOM;
}

/*
	Handles a request
	Input: The request info
	Output: The result of the request
*/
RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
	if (info.id == RQSC_CREATE_ROOM)
		return this->createRoom(info);
	else if (info.id == RQSC_JOIN_ROOM)
		return this->joinRoom(info);
	else if (info.id == RQSC_GET_PLAYERS_IN_ROOM)
		return this->getPlayersInRoom(info);
	else if (info.id == RQSC_GET_ROOMS)
		return this->getRooms(info);
	else if (info.id == RQSC_GET_STATISTICS_USR)
		return this->getStatisticsUsr(info);
	else if (info.id == RQSC_GET_STATISTICS_TOP_USRS)
		return this->getStatisticsTopUsrs(info);
	else if (info.id == RQSC_LOGOUT)
		return this->signout(info);

	return  RequestResult{ JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ RSSC_ERR_MSG_REQST_RELEVANT }, RSSC_ERROR), nullptr };
}

/*
	The fuction disconnects automatically the user, made for server purposes.
	Input: None;
	Output: If the signout was successful and the user was found and removed - bool;
*/
bool MenuRequestHandler::disconnectUser()
{
	return this->_requestHandlerFactory.getLoginManager().signout(this->_loggedUser.getUsername());
}
