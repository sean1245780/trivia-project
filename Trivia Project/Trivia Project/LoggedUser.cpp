#include "LoggedUser.h"

/*
	The function initizalizes the LoggedUser class.
	Input: username = The username to intialize the variables to - const string&;
	Output: None;
*/
LoggedUser::LoggedUser(const string& username) : _username(username)
{
}

/*
	The function returns the username - const.
	Input: None;
	Output: The class' username - string;
*/
string LoggedUser::getUsername() const
{
	return this->_username;
}
