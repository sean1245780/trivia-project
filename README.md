Trivia Project!
By: Brendon Malkis & Sean Tashlik

For downloading MongoDB:
[https://www.mongodb.com/](https://www.mongodb.com/)

For downloading the mongo-c-driver:
[https://github.com/mongodb/mongo-c-driver/releases](https://github.com/mongodb/mongo-c-driver/releases)

For downloading the mongo-cxx-driver (CPP):
[https://github.com/mongodb/mongo-cxx-driver](https://github.com/mongodb/mongo-cxx-driver)

For using the mongo-cxx-driver manual:
[http://mongocxx.org/](http://mongocxx.org/)


Instruction for downloading mongo-cxx-driver (May not be the latest version) -> On Windows:
*  First -> Download the vcpkg file into: C:/ , then rename the folder to vcpkg.
*  Second -> Get inside the vcpkg folder and open from there the cmd with administration abilities.
*  Third -> Run the 2 commands: bootstrap-vcpkg.bat ; vcpkg integrate install
*  Fourth -> Run the 2 commands: vcpkg install mongo-cxx-driver:x86-windows ; vcpkg install mongo-cxx-driver:x64-windows

It will install the needed packages into the folder and connect them to visual studio automatically.

For downloading vcpkg:
[https://github.com/Microsoft/vcpkg](https://github.com/Microsoft/vcpkg)

Have Fun!