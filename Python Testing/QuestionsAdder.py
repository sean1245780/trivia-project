import requests
import os
import sys
import sqlite3
import pymongo
import json
import html

NUM_ARGS = 5
MONGODB_MODE = 0
SQLITE_MODE = 1
MIN_AMOUNT = 1
MAX_AMOUNT = 50
MIN_DIFFICULTY = 0
MAX_DIFFICULTY = 3
DIFFICULTIES ={0: "any", 1: "easy", 2: "medium", 3: "hard"}
SITE = "https://opentdb.com/api.php?"
TABLE_COLLECTION_NAME = "questions"
PARAM_SQLITE_TABLE = "question_obj"


def mongodb_workout(database: str, amount: int, difficulty: str):
    request = SITE + "amount=" + str(amount)

    if difficulty > 0:
        request += "&difficulty=" + str(difficulty)

    print("Waiting for server information...")
    ans = requests.get(request)
    print("Got server information!")

    response_code = ans.json().get("response_code", None)
    ans = ans.json().get("results", None)
    if ans is None or response_code is None:
        print("Invalid server response got back!")
        exit(1)
    elif response_code != 0:
        print("Invalid server response message got back!")
        exit(1)

    client = pymongo.MongoClient()
    db = client[database]
    collection = db[TABLE_COLLECTION_NAME]

    i = 0
    for question in ans:
        for key, value in question.items():
            if key != "incorrect_answers":
                question[key] = html.unescape(value)
            else:
                incorrect_list = question.get(key, None)
                for x in range(len(incorrect_list)):
                    incorrect_list[x] = html.unescape(incorrect_list[x])
                question["incorrect_answers"] = incorrect_list

        if collection.find_one(question) is None:
            collection.insert_one(question)
            i += 1

    print(f"Successfully uploaded to the Mongodb database -> {i} documents!")

    client.close()


def sqlite_workout(database: str, amount: int, difficulty: int):
    request = SITE + "amount=" + str(amount)
    if difficulty > 0:
        request += "&difficulty=" + str(difficulty)
    ans = requests.get(request).json()
    response_code = ans.get("response_code", None)
    ans = ans.get("results", None)
    if ans is None or response_code != 0:
        print("Invalid server response got back!")
        exit(1)

    db = sqlite3.connect(database)

    i = 0
    for question in ans:
        fixed_question = html.escape(json.dumps(question))
        dbcursor = db.cursor()
        dbcursor.execute(f"SELECT * FROM {TABLE_COLLECTION_NAME} WHERE {PARAM_SQLITE_TABLE}=? limit 1;", (fixed_question,))
        ans = dbcursor.fetchall()
        if len(ans) <= 0:
            dbcursor.execute(f"INSERT INTO {TABLE_COLLECTION_NAME} ({PARAM_SQLITE_TABLE}) values (?);", (fixed_question,))
            db.commit()
            i += 1
        dbcursor.close()

    print(f"Successfully uploaded to the Sqlite3 database -> {i} documents!")

    db.close()


def main():
    if len(sys.argv) != NUM_ARGS:
        print(f"Invalid amount of parameters -> python {sys.argv[0]} <Mode (0 - MongoDB, 1 - Sqlite3)> <Database Path (If Mongodb - enter Just Name)> <Amount (1-50)> <Difficulty (any = 0, 1-3, 3 hardest)>")
        exit(1)

    try:
        mode = int(sys.argv[1])
        if not (MONGODB_MODE <= mode <= SQLITE_MODE):
            raise Exception("Mode isn't in the list of the possible options!")
    except ValueError as e:
        print("Invalid mode was given:", e)
        exit(1)
    except Exception as e:
        print("Invalid argument was given:", e)
        exit(1)

    try:
        amount = int(sys.argv[3])
        difficulty = int(sys.argv[4])
        if not (MIN_AMOUNT <= amount <= MAX_AMOUNT):
            raise Exception("Amount isn't in the valid limits!")
        if not (MIN_DIFFICULTY <= difficulty <= MAX_DIFFICULTY):
            raise Exception("Difficult isn't in the valid limits!")
    except ValueError as e:
        print("Invalid amount was given:", e)
        exit(1)
    except Exception as e:
        print("Invalid argument was given:", e)
        exit(1)

    try:
        if mode == MONGODB_MODE:
            client = pymongo.MongoClient()
            db_list = client.list_database_names()
            client.close()
            if sys.argv[2] in db_list:
                for _ in range(50):
                    mongodb_workout(sys.argv[2], amount, difficulty)
            else:
                print("Invalid database was given, its doesn't exists!")
                exit(1)
        elif mode == SQLITE_MODE:
            if os.access(sys.argv[2], os.F_OK):
                sqlite_workout(sys.argv[2], amount, difficulty)
            else:
                print("Invalid path was given for a proper file!")
                exit(1)
        else:
            print("Invalid mode was entered, outside of the proper limits!")
            exit(1)
    except Exception as e:
        print("Main exception occurred:", e)
        exit(1)

    print("Finished successfully the questions uploading!")


if __name__ == '__main__':
    main()

