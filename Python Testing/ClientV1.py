import socket
import json

SERVER_IP = "127.0.0.1"
SERVER_PORT = 9512
MSG = "Hello"
responseStatusCode = {"RSSC_ERROR": 0, "RSSC_LOGIN": 1, "RSSC_SIGNUP": 2, "RSSC_LOGOUT": 3, "RSSC_ILLGL_STR": 4, "RSSC_ILLGL_USERN": 5,
                      "RSSC_ILLGL_PASSWRD": 6, "RSSC_ILLGL_EMAIL": 7, "RSSC_ILLGL_ADDRS": 8, "RSSC_ILLGL_PHONEN": 9,
                      "RSSC_ILLGL_BRTHD_PRM": 10, "RSSC_WRONG_LOGIN_PRM": 11, "RSSC_ALRDY_TKN": 12, "RSSC_USER_ALRDY_CNNCTD": 13,
                      "RSSC_GET_ROOMS": 14, "RSSC_GET_PLAYERS_IN_ROOM": 15, "RSSC_JOIN_ROOM": 16, "RSSC_GET_STATISTICS_USR": 17,
                      "RSSC_GET_STATISTICS_TOP_USRS": 18, "RSSC_CREATE_ROOM": 19, "RSSC_ERR_USR_NOT_EXST": 20, "RSSC_ERR_ROOM_NOT_EXST": 21}

requestStatusCode = {"RQSC_LOGOUT": 128, "RQSC_SIGNUP": 129, "RQSC_LOGIN": 130, "RQSC_CREATE_ROOM": 131, "RQSC_JOIN_ROOM": 132, "RQSC_GET_STATISTICS_USR" : 133,
                     "RQSC_GET_STATISTICS_TOP_USRS" : 134, "RQSC_GET_PLAYERS" : 135, "RQSC_GET_ROOMS" : 136}

STATE_NAME = ["Error", "Login", "Sign Up", "Logout", "Illegal input string", "Illegal username string", "Illegal password string",
              "Illegal email string", "Illegal address string", "Illegal phone number string", "Illegal birth date string", "Wrong login parameters",
              "Username already taken", "User already connected", "Get rooms", "Get players in room string", ""]
FAIL = 0
SUCCESS = 1
EXIT_CODE = 0
LOGIN_CODE = 1
SIGNUP_CODE = 2
LOGOUT_CODE = 1
JOIN_ROOM_CODE = 2
GET_ROOMS_CODE = 3
CREATE_ROOM_CODE = 4
GET_PLAYERS_IN_ROOM_CODE = 5
GET_STATISTICS_USR_CODE = 6
GET_STATISTICS_TOP_USRS_CODE = 7
CHAR_LIMIT = 127

STATUS_TXT = "status"
MESSAGE_TXT = "message"

# STATES = {0: 0, 1: 1, 2: 1, 3: 2}  # The given current states -> If the response is valid and the current state is 1 it will be raised 2
# The states texts are going by the response status code
ERROR_STATES_TXT = {0: "Error was made by the server or the user!", 1: "Login failed due an error that occurred!",
                    2: "Signup failed due an error that occurred!", 4: "Input contains illegal characters!", 5: "Illegal username was entered!",
                    6: "Illegal password was entered!", 7: "Illegal email was entered!", 8: "Illegal address was entered!", 9: "Illegal phone number was entered!",
                    10: "Illegal birth date was entered!", 11: "Wrong username or password!", 12: "Username already taken!", 13: "User already connected to the server!",
                    14: "No room exists!", 15: "Couldn't get the players in room -> Room doesn't exist!", 16: "Couldn't join the room, the room doesn't exists!",
                    17: "Couldn't get the statistics of the user!", 18: "Couldn't get the statistics of the top 3 users!", 19: "Couldn't create the room!", 20: "User doesn't exists!",
                    21: "Room doesn't exists!"}

SUCCESS_STATES_TXT = {0: "Successfully back due Login / Signup state!", 1: "Was successfully logged in!", 2: "Was successfully signed up!", 14: "Successfully got the list of rooms!",
                      15: "Successfully got the players in the wanted room!", 16: "Successfully joined the wanted room!", 17: "Successfully got the user's statistics!",
                      18: "Successfully got the top 3 users' statistics!", 19: "Successfully created the wanted room!"}

curr_state = 1  # Default state is 1
prev_state = 1
DEFAULT_STATE = 1

# Format: Response (status code, msg), - Logging, Parameters
AUTO_QUESTIONS = [((10, "{\"status\":0}"), True, "Blin", "login123"), ((6, "{\"status\":0}"), False, "Blin", "login123", "som @gm.c", "(d, 5, p)", "05-123", "01/02/2002"), ((4, "{\"status\":0}"), True, "bl", "login123"),
                  ((5, "{\"status\":0}"), False, "dope123", "logi3", "som@gm.c", "(d, 5, p)", "05-123", "01/02/2002"), ((7, "{\"status\":0}"), False, "dope123o", "logi3#&Nope", "f@f.f.f.c", "(d, 10,p)", "048-11223", "01/02/2002"),
                  ((6, "{\"status\":0}"), False, "dopey12", "logi3in$1", "som@1gm.c", "(d, 5, p)", "05-123", "01/02/2002"), ((6, "{\"status\":0}"), False, "dopey123", "logi3in$1", "som@gm.c.", "(d, 5, p)", "05-123", "01/02/2002"),
                  ((4, "{\"status\":0}"), False, "noping$%1", "logi3in$1", "som@gm.c.v", "(d, 5, p)", "05-123", "01/02/2002"), ((7, "{\"status\":0}"), False, "noping1123", "logi3in$1", "som@gm.c.v", "(dope, nope, p)", "05-123", "01/02/2002"),
                  ((8, "{\"status\":0}"), False, "noping1123", "logi3in$1", "som@gm.c.v", "(dope, 12, p)", "550-1234", "01/02/2002"), ((11, "{\"status\":0}"), False, "dope123o", "logi3in$1", "som@gm.c.v", "(dope, 6, p)", "05-123", "01/02/2002"),
                  ((9, "{\"status\":0}"), False, "noping1123h", "logi3in$1", "som@gm.c.v.d", "(dope, 12, p)", "055-1234", "01/2/2002"), ((1, "{\"status\":1}"), True, "dope123o", "logi3#&Nope")]


def connect_to_server():
    """
    The function connects to the server.
    :return: The socket
    :rtype: socket.socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    return sock


def get_message(sock: socket):
    """
    The function gets the message from the server.
    :param sock: The socket to use
    :type sock: socket
    :return: The message sent by the server
    :rtype: str
    """
    server_message = sock.recv(2048)
    if len(server_message) == 0:
        raise Exception("Socket Closed From Server Side!")
    server_message = server_message.decode('latin-1')
    return server_message


def send_data(sock: socket, data: str):
    """
    The function sends data to the server.
    :param sock: The socket to use
    :param: The data to send
    :type sock: socket
    :type data: str
    """
    sock.sendall(data.encode('latin-1'))


def parse_data_send(data: dict):
    """
    The function parses the given data by the given protocol.
    :param data: the data to send to the server
    :type data : dict
    :return: The parsed data
    :rtype: dict
    """

    parsed = {}

    status_code = data.get("code", None)

    for key, value in data.items():
        if key != "code":
            parsed[key] = value

    msg = json.dumps(parsed)

    return "".join([chr(status_code), "".join([chr((len(msg) >> i * 8) & 0xFF) for i in range(4)]), msg])


def parse_data_get(data: str):
    """
    The function parses the given data by the given protocol.
    :param data: The data to extract from the parameters
    :return: The data
    :rtype: tuple
    """
    try:
        json_val = json.loads(data[5:])
    except Exception as e:
        raise Exception("Invalid data to json transformation! -> {}".format(e))
    return ord(data[0]), sum([(ord(data[i + 1]) << 8 * i) for i in range(4)]), json_val


def get_input():
    """
    The function get the needed input for the client.
    :return: The given data
    :rtype: dict
    """
    try:
        value = None
        global curr_state
        if curr_state == 1:
            while value is None or value < EXIT_CODE or value > SIGNUP_CODE:
                value = int(input("Enter your choice ({} - Exit, {} - Login, {} - Signup): ".format(EXIT_CODE, LOGIN_CODE, SIGNUP_CODE)))

            if value == LOGIN_CODE:
                username = input("Enter the username: ")
                password = input("Enter the password: ")
                value = {"code": requestStatusCode["RQSC_LOGIN"], "username" : username, "password" : password}
            elif value == SIGNUP_CODE:
                username = input("Enter the username (At least length of 3, Only: letters and numbers, starting with letters): ")
                password = input("Enter the password (At least length of 8, Only: a-z, A-Z, 0-9, *, &, ^, %, $, #, @, !): ")
                email = input("Enter the email: ")
                address = input("Enter the address (Format -> (Street, Home number, City)): ")
                phone = input("Enter the phone number (Format -> prefix-number): ")
                birth_date = input("Enter your birth date (Minimum year: 1900, Format -> DD/MM/YY or DD.MM.YY): ")
                value = {"code": requestStatusCode["RQSC_SIGNUP"], "username" : username, "password" : password, "address" : address, "email" : email, "phone_num" : phone, "birth_date": birth_date}
        elif curr_state == 2:
            print("HI! You entered the next state!")
            value = int(input("Enter your choice ({} - Logout, {} - Join Room, {} - Get Rooms, {} - Create Room, {} - Get Players In Room, {} - Get Statistics of User, {} - Get Statistics of Top Users): "
                              .format(LOGOUT_CODE, JOIN_ROOM_CODE, GET_ROOMS_CODE, CREATE_ROOM_CODE, GET_PLAYERS_IN_ROOM_CODE, GET_STATISTICS_USR_CODE, GET_STATISTICS_TOP_USRS_CODE)))

            if value == LOGOUT_CODE:
                value = {"code": requestStatusCode["RQSC_LOGOUT"]}
            elif value == JOIN_ROOM_CODE:
                value = {"code": requestStatusCode["RQSC_JOIN_ROOM"], "roomId": int(input("Enter the room id you want to enter: "))}
            elif value == GET_ROOMS_CODE:
                value = {"code": requestStatusCode["RQSC_GET_ROOMS"]}
            elif value == CREATE_ROOM_CODE:
                value = {"code": requestStatusCode["RQSC_CREATE_ROOM"], "roomName": input("Enter the room's name: "), "maxUsers": int(input("Enter the max amount of users (1 <= num <= 20): ")),
                          "questionsCount": int(input("Enter the amount of question you want (3 <= num <= 30): ")), "answerTimeout": int(input("Enter the answer timeout (5 <= num <= 60): "))}
            elif value == GET_PLAYERS_IN_ROOM_CODE:
                value = {"code": requestStatusCode["RQSC_GET_PLAYERS"], "roomId": int(input("Enter the room you want to get all the players from: "))}
            elif value == GET_STATISTICS_USR_CODE:
                value = {"code": requestStatusCode["RQSC_GET_STATISTICS_USR"]}
            elif value == GET_STATISTICS_TOP_USRS_CODE:
                value = {"code": requestStatusCode["RQSC_GET_STATISTICS_TOP_USRS"]}
        elif curr_state == 3:
            print("In rooms Mode!")
            exit(1)
        else:
            print("Invalid state!")
            exit(1)
    except Exception as e:
        print(e)
        return None
    return value


def handle_state_change(id_msg: int, state: dict):
    """
    The function handle the state change by the given response from the server.
    :param state: The response given by the server
    :param id_msg: The id of the response to check by:
    :type state: dict
    :type id_msg: dict
    :return: If the request was successful
    :rtype: bool
    """

    global curr_state, prev_state

    if id_msg != responseStatusCode.get("RSSC_ERROR", 0):
        if state is None:
            raise Exception("Invalid state given!")
        elif state.get(MESSAGE_TXT, None) is not None:
            print("The server returned error:", state.get(MESSAGE_TXT, " -Couldn't read the response!-"))
        if id_msg == responseStatusCode.get("RSSC_LOGIN") or id_msg == responseStatusCode.get("RSSC_SIGNUP"):
            if state.get(STATUS_TXT, FAIL) == SUCCESS:
                prev_state = curr_state
                curr_state += 1
                return True
        elif id_msg == responseStatusCode.get("RSSC_LOGOUT"):
            prev_state = curr_state
            curr_state = DEFAULT_STATE
            return True
        elif id_msg in [responseStatusCode.get("RSSC_GET_ROOMS"), responseStatusCode.get("RSSC_GET_PLAYERS_IN_ROOM"), responseStatusCode.get("RSSC_GET_STATISTICS_USR"),
                        responseStatusCode.get("RSSC_GET_STATISTICS_TOP_USRS")]:
            if state.get(STATUS_TXT, FAIL) == SUCCESS:
                return True
        elif id_msg in [responseStatusCode.get("RSSC_CREATE_ROOM"), responseStatusCode.get("RSSC_JOIN_ROOM")]:
            if state.get(STATUS_TXT, FAIL) == SUCCESS:
                prev_state = curr_state
                curr_state += 1
                return True
    return False


def response_state_change(id_msg: int, state_change: bool):
    """
    The function prints the given response by the server by the given status.
    :param id_msg: The status code of the response
    :param state_change: If the state was changed
    :type id_msg: int
    :type state_change: bool
    """

    if state_change is True:
        global curr_state, prev_state
        if prev_state > curr_state:
            print(SUCCESS_STATES_TXT.get(0, "-Error occurred in printing message! (Success backwards state)-"))
        else:
            print(SUCCESS_STATES_TXT.get(id_msg, "-Error occurred in printing message! (Success forward state)-"))
    elif state_change is False:
        print(ERROR_STATES_TXT.get(id_msg, "-Error occurred in printing message! (Fail state)-"))
    else:
        raise Exception("Invalid state change given!")


def main():
    # The main that connects and speaks with the server

    sock = connect_to_server()

    try:
        while True:
            ans = None
            while ans is None:
                ans = get_input()
            if ans == EXIT_CODE:
                break
            send_data(sock, parse_data_send(ans))
            print()  # Making space between the questions and the answers
            id_msg, length, msg = parse_data_get(get_message(sock))
            response_state_change(id_msg, handle_state_change(id_msg, msg))
            print("Id: {}, Length: {}\nMessage: {}".format(id_msg, length, msg))
            print()  # Making space between the states questions
        sock.close()
        print("Thanks for using the Trivia app!")
    except Exception as e:
        print("Main Exception:", e)


def auto_main():
    # Auto tester of the client + server

    try:
        for query in AUTO_QUESTIONS:
            sock = connect_to_server()
            response, *request = query
            request = tuple(request)
            logging, *request = request
            request = tuple(request)
            send_data(sock, parse_data_send(*request, logginning=logging))
            id_msg, length, msg = parse_data_get(get_message(sock))
            final_response = (response[0], len(response[1]), json.loads(response[1]))
            print("Id: {} -> {}, Length: {}\nMessage: {}".format(id_msg, STATE_NAME[id_msg], length, msg))
            print("Valuing: id_msg = {}, length = {}, msg = {}".format(final_response[0], final_response[1], final_response[2]))
            print("Final: id_msg = {}, length = {}, msg = {}".format(final_response[0] == id_msg, final_response[1] == length, final_response[2] == msg))
            print("\n")  # Making space between the states questions
            sock.close()
        print("Thanks for using the Trivia app!")
    except Exception as e:
        print("Main Exception:", e)


if __name__ == '__main__':
    main()
    #auto_main()
