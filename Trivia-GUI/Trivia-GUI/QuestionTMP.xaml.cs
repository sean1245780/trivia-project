﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class QuestionTMP : Page
    {
        public enum Phase { SignUp, LogIn, CreateRoom, JoinRoom };
        private static bool inits = false;
        private static StackPanel[] sps = new StackPanel[3];

        public string usernameTxt = "";

        public QuestionTMP(Phase phase)
        {
            InitializeComponent();

            if(!inits)
            {
                sps[0] = initiateSignup();
                sps[1] = initiateLogin();
                sps[2] = initiateCreateRoom();
                inits = true;
            }

            if(phase == Phase.SignUp)
            {
                this.titleTXT.Text = "Sign Up";
                this.submitBTN.Click += ((object sender, RoutedEventArgs e)=> {
                    SignupRequest signupR = new SignupRequest();
                    Func<string, string> getData = (str) => 
                    {
                        //object obj = sps[0].FindName(str);
                        //if (obj is TextBox)
                        //{
                        //    Console.WriteLine("agagdd");
                        //    return (obj as TextBox).Text;
                        //}
                        //else if(obj is PasswordBox)
                        //{
                        //    return (obj as PasswordBox).Password;
                        //}
                        //else if(obj is DatePicker)
                        //{
                        //    return (obj as DatePicker).SelectedDate.Value.ToString();
                       
                        foreach(object panel in sps[0].Children)
                        {
                            if(panel is StackPanel)
                                foreach(object box in (panel as StackPanel).Children)
                                {
                                    if (box is TextBox && (box as TextBox).Name == str)
                                        return (box as TextBox).Text;
                                    else if (box is PasswordBox && (box as PasswordBox).Name == str)
                                        return (box as PasswordBox).Password;
                                    else if (box is DatePicker && (box as DatePicker).Name == str)
                                        return (box as DatePicker).SelectedDate.Value.ToString();
                                }
                        }

                        return "";
                    };

                    signupR.username = getData(Constants.signinFieldsNames[0]);
                    signupR.password = getData(Constants.signinFieldsNames[1]);
                    signupR.email = getData(Constants.signinFieldsNames[2]);
                    signupR.address = getData(Constants.signinFieldsNames[3]);
                    signupR.phoneNum = getData(Constants.signinFieldsNames[4]);
                    signupR.birthDate = getData(Constants.signinFieldsNames[5]);
                    string dateits = signupR.birthDate.Substring(0, (signupR.birthDate.IndexOf(" ")));
                    Console.WriteLine(dateits);
                    int dayp = dateits.IndexOf("/"), monthp = dateits.Substring(dayp + 1).IndexOf("/");
                    string days = dateits.Substring(0, dayp), months = dateits.Substring(dayp + 1, monthp), years = dateits.Substring(dayp + monthp + 2);
                    if (days.Length < 2) { days = "0" + days; }
                    if (months.Length < 2) { months = "0" + months; }
                    if (years.Length == 3) { years = "0" + years; }
                    else if (years.Length == 2) { years = "00" + years; }
                    else if (years.Length == 1) { years = "000" + years; }
                    signupR.birthDate = (months + "/" + days + "/" + years);

                    usernameTxt = signupR.username;

                    (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_SIGNUP, JsonConvert.SerializeObject(signupR));
                });

                sps[0] = clearStackPanel(sps[0]);
                this.scrollV.Content = sps[0];
            }
            else if (phase == Phase.LogIn)
            {
                this.titleTXT.Text = "Login";
                this.submitBTN.Click += ((object sender, RoutedEventArgs e) => {
                    LoginRequest loginR = new LoginRequest();
                    Func<string, string> getData = (str) =>
                    {
                        foreach (object panel in sps[1].Children)
                        {
                            if (panel is StackPanel)
                                foreach (object box in (panel as StackPanel).Children)
                                {

                                    if (box is TextBox && (box as TextBox).Name == str)
                                        return (box as TextBox).Text;
                                    else if (box is PasswordBox && (box as PasswordBox).Name == str)
                                        return (box as PasswordBox).Password;
                                 }
                        }

                        return "";
                    };

                    loginR.username = getData(Constants.signinFieldsNames[0]);
                    loginR.password = getData(Constants.signinFieldsNames[1]);

                    usernameTxt = loginR.username;

                    (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_LOGIN, JsonConvert.SerializeObject(loginR));
                });
                sps[1] = clearStackPanel(sps[1]);
                this.scrollV.Content = sps[1];
            }
            else if(phase == Phase.CreateRoom)
            {

                this.titleTXT.Text = "Create Room";
                this.submitBTN.Click += ((object sender, RoutedEventArgs e) =>
                {
                    CreateRoomRequest request = new CreateRoomRequest();
                    Func<string, string> getData = (str) =>
                    {
                        foreach (object panel in sps[2].Children)
                        {
                            if (panel is StackPanel)
                                foreach (object box in (panel as StackPanel).Children)
                                {

                                    if (box is TextBox && (box as TextBox).Name == str)
                                        return (box as TextBox).Text;
                                }
                        }

                        return "";
                    };

                    request.roomName = getData(Constants.createRoomFieldsNames[0]);
                    request.maxUsers = getData(Constants.createRoomFieldsNames[1]);
                    request.questionsCount = getData(Constants.createRoomFieldsNames[2]);
                    request.answerTimeout = getData(Constants.createRoomFieldsNames[3]);

                    (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_CREATE_ROOM, JsonConvert.SerializeObject(request));
                });

                sps[2] = clearStackPanel(sps[2]);
                this.scrollV.Content = sps[2];
            }
            else
            {
                throw new Exception("Invalid phase was given to initiate QuestionTMP!");
            }
        }

        private StackPanel initiateSignup()
        {
            StackPanel sp1 = initiateLogin();
            StackPanel[] spBx = new StackPanel[4];
            TextBlock[] txtblk = new TextBlock[4];
            TextBox[] textbx = new TextBox[3];
            DatePicker dtpkr = new DatePicker();

            for (int i = 0; i < spBx.Length; i++)
            {
                spBx[i] = new StackPanel();
                txtblk[i] = new TextBlock();

                txtblk[i].Text = Constants.signinFieldsLabels[i + 2];
                txtblk[i].FontSize = 30;
                txtblk[i].Style = this.FindResource("MylF") as Style;
                txtblk[i].VerticalAlignment = VerticalAlignment.Center;
                txtblk[i].HorizontalAlignment = HorizontalAlignment.Center;
                txtblk[i].Foreground = Brushes.Black;
                spBx[i].Children.Add(txtblk[i]);

                if(i != 3)
                {
                    textbx[i] = new TextBox();
                    textbx[i].Name = Constants.signinFieldsNames[i + 2];
                    textbx[i].FontSize = 30;
                    textbx[i].HorizontalAlignment = HorizontalAlignment.Stretch;
                    textbx[i].HorizontalContentAlignment = HorizontalAlignment.Center;
                    textbx[i].VerticalContentAlignment = VerticalAlignment.Center;
                    textbx[i].Margin = new Thickness(10);
                    spBx[i].Children.Add(textbx[i]);
                }
            }

            dtpkr.Name = Constants.signinFieldsNames[5];
            dtpkr.FontSize = 30;
            dtpkr.SelectedDate = new DateTime(DateTime.Now.Ticks);
            dtpkr.HorizontalAlignment = HorizontalAlignment.Center;
            dtpkr.HorizontalContentAlignment = HorizontalAlignment.Center;
            dtpkr.VerticalContentAlignment = VerticalAlignment.Center;
            dtpkr.Margin = new Thickness(10);
            dtpkr.SelectedDateFormat = DatePickerFormat.Short;
            spBx[3].Children.Add(dtpkr);

            for (int i = 0; i < spBx.Length; i++)
            {
                sp1.Children.Add(spBx[i]);
            }

            return sp1;
        }

        private StackPanel initiateLogin()
        {
            StackPanel sp1 = new StackPanel();
            StackPanel[] spBx = new StackPanel[2];
            TextBlock[] txtblk = new TextBlock[2];
            TextBox txtbx = new TextBox();
            PasswordBox psbx = new PasswordBox();

            for (int i = 0; i < spBx.Length; i++)
            {
                spBx[i] = new StackPanel();
                txtblk[i] = new TextBlock();

                txtblk[i].Text = Constants.signinFieldsLabels[i];
                txtblk[i].FontSize = 30;
                txtblk[i].Style = this.FindResource("MylF") as Style;
                txtblk[i].VerticalAlignment = VerticalAlignment.Center;
                txtblk[i].HorizontalAlignment = HorizontalAlignment.Center;
                txtblk[i].Foreground = Brushes.Black;
                spBx[i].Children.Add(txtblk[i]);
            }

            txtbx.Name = Constants.signinFieldsNames[0];
            txtbx.FontSize = 30;
            txtbx.HorizontalAlignment = HorizontalAlignment.Stretch;
            txtbx.HorizontalContentAlignment = HorizontalAlignment.Center;
            txtbx.VerticalContentAlignment = VerticalAlignment.Center;
            txtbx.Margin = new Thickness(10);

            psbx.Name = Constants.signinFieldsNames[1];
            psbx.FontSize = 30;
            psbx.HorizontalAlignment = HorizontalAlignment.Stretch;
            psbx.HorizontalContentAlignment = HorizontalAlignment.Center;
            psbx.VerticalContentAlignment = VerticalAlignment.Center;
            psbx.Margin = new Thickness(10);

            spBx[0].Children.Add(txtbx);
            spBx[1].Children.Add(psbx);

            sp1.Children.Add(spBx[0]);
            sp1.Children.Add(spBx[1]);

            return sp1;
        }

        private StackPanel initiateCreateRoom()
        {

            StackPanel sp2 = new StackPanel();
            StackPanel[] spBx = new StackPanel[4];
            TextBox[] txtBx = new TextBox[4];
            TextBlock[] txtBlk = new TextBlock[4];

            for(int i = 0; i < spBx.Length; i++)
            {

                spBx[i] = new StackPanel();
                txtBlk[i] = new TextBlock();
                txtBx[i] = new TextBox();

                txtBlk[i].Text = Constants.createRoomFieldsLabels[i];
                txtBlk[i].FontSize = 30;
                txtBlk[i].Style = this.FindResource("MylF") as Style;
                txtBlk[i].VerticalAlignment = VerticalAlignment.Center;
                txtBlk[i].HorizontalAlignment = HorizontalAlignment.Center;
                txtBlk[i].Foreground = Brushes.Black;
                spBx[i].Children.Add(txtBlk[i]);

                txtBx[i].Name = Constants.createRoomFieldsNames[i];
                txtBx[i].FontSize = 30;
                txtBx[i].HorizontalAlignment = HorizontalAlignment.Stretch;
                txtBx[i].HorizontalContentAlignment = HorizontalAlignment.Center;
                txtBx[i].VerticalContentAlignment = VerticalAlignment.Center;
                txtBx[i].Margin = new Thickness(10);
                spBx[i].Children.Add(txtBx[i]);

                sp2.Children.Add(spBx[i]);
            }

            return sp2;
        }

        private StackPanel clearStackPanel(StackPanel sp)
        {
            foreach (object child1 in sp.Children)
            {
                if(child1 is StackPanel)
                {
                    foreach(object child2 in (child1 as StackPanel).Children)
                    {
                        if(child2 is TextBox)
                        {
                            (child2 as TextBox).Text = "";
                        }
                        else if (child2 is PasswordBox)
                        {
                            (child2 as PasswordBox).Password = "";
                        }
                    }
                }
            }

            return sp;
        }
        private void setErrorMsgStart(object sender, EventArgs e)
        {
            ErrorMsg.Visibility = Visibility.Collapsed;
        }
    }
}
