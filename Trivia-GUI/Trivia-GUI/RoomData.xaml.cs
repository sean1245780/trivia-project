﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for RoomData.xaml
    /// </summary>
    public partial class RoomData : StackPanel
    {

        private uint maxUsers = 0, currUsers = 0, qCount = 0, timeAns = 0;
        private string roomName = "";
        public uint Id = 0;
        public roomState isActive = roomState.InitMode;

        public RoomData(uint id, string name, uint maxUsers, uint currUsers, uint qCount, uint time, roomState isactive)
        {
            InitializeComponent();

            this.Id = id;
            this.RoomName = name;
            this.MaxUsers = maxUsers;
            this.CurrUsers = currUsers;
            this.QCount = qCount;
            this.TimeAns = time;
            this.isActive = isactive;
        }

        public RoomData(RoomDataInfo rdi)
        {
            InitializeComponent();

            this.Id = rdi.roomId;
            this.RoomName = rdi.roomName;
            this.MaxUsers = rdi.maxPlayers;
            this.CurrUsers = rdi.numPlayers;
            this.QCount = rdi.questionsCount;
            this.TimeAns = rdi.timePerQuestion;
            this.isActive = (roomState)rdi.isActive;
        }

        public uint MaxUsers
        {
            set
            {
                this.maxUsers = value;
                this.numUsrs.Text = "Users: " + this.currUsers + "/" + value;
            }
            get { return this.maxUsers; }
        }
        public uint CurrUsers
        {
            set
            {
                this.currUsers = value;
                this.numUsrs.Text = "Users: " + value + "/" + this.maxUsers;
            }
            get { return this.currUsers; }
        }

        public uint QCount
        {
            set
            {
                this.qCount = value;
                this.qstnAmnt.Text = "Questions Count: " + value;
            }
            get { return this.qCount; }
        }

        public string RoomName
        {
            set
            {
                this.roomName = value;
                this.roomTtl.Text = value;
            }
            get { return this.roomName; }
        }

        public uint TimeAns
        {
            set
            {
                this.timeAns = value;
                this.timePQ.Text = "Question Time: " + value;
            }
            get { return this.timeAns; }
        }

    }
}
