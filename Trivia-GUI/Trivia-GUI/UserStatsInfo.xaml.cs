﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for UserStatsInfo.xaml
    /// </summary>
    public partial class UserStatsInfo : Page
    {
        private double ranking = 0, avgTimePerQuestion = 0;
        private uint numGames = 0, numAnswers = 0, correctAnswers = 0, wrongAnswers = 0, scorePoints = 0;

        public UserStatsInfo()
        {
            InitializeComponent();

            Ranking = 0;
            AvgTimePerQuestion = 0;
            NumGames = 0;
            NumAnswers = 0;
            CorrectAnswers = 0;
            WrongAnswers = 0;
            ScorePoints = 0;

            try
            {
                (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_GET_STATISTICS_USR, "{}");
            }
            catch (Exception e)
            {
                this.ErrorMsg.Content = e.Message;
            }
        }

        public double Ranking
        {
            set
            {
                ranking = value;
                this.RankingP.Text = "Ranking: " + ranking;
            }
            get { return ranking; }
        }

        public double AvgTimePerQuestion
        {
            set
            {
                avgTimePerQuestion = value;
                this.AvgTPQP.Text = "Avg Time Per Question: " + avgTimePerQuestion;
            }
            get { return avgTimePerQuestion; }
        }

        public uint NumGames
        {
            set
            {
                numGames = value;
                this.GameNumP.Text = "Amount of Games: " + numGames;
            }
            get { return numGames; }
        }

        public uint NumAnswers
        {
            set
            {
                numAnswers = value;
                this.AnsNumP.Text = "Amount of Answers: " + numAnswers;
            }
            get { return numAnswers; }
        }

        public uint CorrectAnswers
        {
            set
            {
                correctAnswers = value;
                this.CrrctAnsP.Text = "Amount of Correct Answers: " + correctAnswers;
            }
            get { return correctAnswers; }
        }

        public uint WrongAnswers
        {
            set
            {
                wrongAnswers = value;
                this.WrongAnsP.Text = "Amount of Wrong Answers: " + wrongAnswers;
            }
            get { return wrongAnswers; }
        }

        public uint ScorePoints
        {
            set
            {
                scorePoints = value;
                this.ScrPointsP.Text = "Score Points: " + scorePoints;
            }
            get { return scorePoints; }
        }
    }
}
