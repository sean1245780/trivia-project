﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for PageInfo.xaml
    /// </summary>
    public partial class RoomsInfo : Page
    {
        private int block = 0;
        public RoomsInfo()
        {
            InitializeComponent();
            initializeJoinRoom();
        }

        private void initializeJoinRoom()
        {
            this.refreshBTN.Click += (object sender, RoutedEventArgs e) => {
                (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_GET_ROOMS, "{}");
            };

            (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_GET_ROOMS, "{}");
        }
    }
}
