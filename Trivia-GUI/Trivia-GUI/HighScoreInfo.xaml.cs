﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for HighScoreInfo.xaml
    /// </summary>
    public partial class HighScoreInfo : Page
    {
        private string[] username = null;
        private double[] ranking = null;

        public HighScoreInfo()
        {
            InitializeComponent();
            username = new string[3];
            ranking = new double[3];

            for(int i = 0; i < username.Length; i++)
            {
                username[i] = "None";
            }

            try
            {
                (Application.Current.MainWindow as MainWindow).sendData(requestStatusCode.RQSC_GET_STATISTICS_TOP_USRS, "{}");
            }
            catch(Exception e)
            {

                this.ErrorMsg.Content = e.Message;
            }
        }

        public string[] Username
        {
            set
            {
                username = value;
                for (int i = 0; i < username.Length; i++)
                {
                    if(username[i] == null)
                    {
                        username[i] = "-N/A-";
                    }
                }

                this.FirstPlace.Content = "1 -> " + username[0] + " : " + ranking[0];
                this.SecondPlace.Content = "2 -> " + username[1] + " : " + ranking[1];
                this.ThirdPlace.Content = "3 -> " + username[2] + " : " + ranking[2];
            }
            get { return username; }
        }

        public double[] Ranking
        {
            set
            {
                ranking = value;
                this.FirstPlace.Content = "1 -> " + username[0] + " : " + ranking[0];
                this.SecondPlace.Content = "2 -> " + username[1] + " : " + ranking[1];
                this.ThirdPlace.Content = "3 -> " + username[2] + " : " + ranking[2];
            }
            get { return ranking; }
        }

    }
}
