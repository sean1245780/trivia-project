﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private enum State { LogSgn, Menu, RoomChs, RoomCrt, RoomJin, RoomPly }
        private static State gameState = State.LogSgn;
        private string _usrnameObj = "";
        private static Communicator cmnct = Communicator.getInstance();

        public MainWindow()
        {
            InitializeComponent();

            this.TtlGame.MouseDown += (object sender, MouseButtonEventArgs e)=>
            {
                if (this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                { (this.PagesUse.Content as HomePage).AnimationWork = false; }
                this.PagesUse.Content = null;
                HomePage hp = new HomePage();
                hp.AnimationWork = true;
                this.PagesUse.Content = hp;
            };


            MenuInitializer();
            
            //QuestionTMP qtmp = new QuestionTMP(QuestionTMP.Faze.LogIn);
            //this.Content = qtmp;
            //createMenu();

        }

        private string usernameObject
        {
            set
            {
                _usrnameObj = value;

                if(_usrnameObj == null || _usrnameObj == "")
                {
                    this.UsrnmTxt.Text = "Username: -N/A-";
                }
                else
                {
                    this.UsrnmTxt.Text = "Username: " + _usrnameObj;
                }
            }
            get { return _usrnameObj; }
        }

        //this function creates the styles and the menu
        //private void createMenu()
        //{

        //    Grid menu = new Grid();

        //    menu.SetValue(Grid.ColumnProperty, 0);

        //    RowDefinition userData = new RowDefinition();
        //    userData.Height = new GridLength(600 / 7);

        //    RowDefinition buttons = new RowDefinition();
        //    buttons.Height = new GridLength(600 / 7 * 6);

        //    menu.RowDefinitions.Add(userData);
        //    menu.RowDefinitions.Add(buttons);

        //    StackPanel userDataPanel = new StackPanel();
        //    userDataPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
        //    userDataPanel.VerticalAlignment = VerticalAlignment.Stretch;
        //    userDataPanel.Background = Brushes.WhiteSmoke;
        //    userDataPanel.SetValue(Grid.RowProperty, 0);
        //    userDataPanel.SetValue(Grid.RowSpanProperty, 2);

        //    TextBlock title = new TextBlock();
        //    title.Text = "Trivia";
        //    title.Foreground = Brushes.Red;
        //    title.HorizontalAlignment = HorizontalAlignment.Center;
        //    title.FontSize = 30;

        //    userDataPanel.Children.Add(title);

        //    for (int i = 0; i < Constants.userData.Length; i++)
        //    {



        //        TextBlock t = new TextBlock();
        //        t.Text = Constants.userData[i];
        //        t.FontSize = 15;
        //        t.Foreground = Brushes.Black;
        //        t.Margin = new Thickness(0, 0, 16, 0);

        //        userDataPanel.Children.Add(t);
        //    }

        //    StackPanel buttonsPanel = new StackPanel();
        //    buttonsPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
        //    buttonsPanel.VerticalAlignment = VerticalAlignment.Stretch;
        //    buttonsPanel.SetValue(Grid.RowProperty, 1);
        //    buttonsPanel.Background = Brushes.White;

        //    for(int i = 0; i < Constants.buttonLabels.Length; i++)
        //    {

        //        TextBlock t = new TextBlock();
        //        t.Foreground = Brushes.Black;
        //        t.Text = Constants.buttonLabels[i];
        //        t.FontSize = 30;
        //        t.VerticalAlignment = VerticalAlignment.Center;
        //        t.HorizontalAlignment = HorizontalAlignment.Center;

        //        Border b = new Border();
        //        b.CornerRadius = new CornerRadius(10);
        //        b.BorderThickness = new Thickness(2);
        //        b.BorderBrush = Brushes.Black;
        //        b.Background = Brushes.LightBlue;
        //        b.Height = 60;
        //        b.Margin = new Thickness(10, 16, 10, 0);
        //        b.Child = t;

        //        buttonsPanel.Children.Add(b);
        //    }

        //    ScrollViewer scroll = new ScrollViewer();
        //    scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        //    scroll.SetValue(Grid.RowProperty, 1);
        //    scroll.Content = buttonsPanel;

        //    menu.Children.Add(scroll);
        //    menu.Children.Add(userDataPanel);

        //    this.Window.Children.Add(menu);
        //}

        /*<Border  CornerRadius="10" BorderThickness="2" BorderBrush="Black" Margin="10,16,10,0" Height="60" Background="LightBlue">
                        <TextBlock TextWrapping="Wrap" VerticalAlignment="Center" HorizontalAlignment="Center" Style="{StaticResource BerlinF}" FontSize="30" Text="Sign Up"/>
                    </Border>
                    */

        private void MenuInitializer()
        {
            Button[] btns = new Button[2];

            for (int i = 0; i < btns.Length; i++)
            {
                btns[i] = new Button();
                btns[i].Name = Constants.buttonMenuNames[i];
                btns[i].Style = this.FindResource("MenuButton") as Style;
                btns[i].HorizontalAlignment = HorizontalAlignment.Stretch;
                btns[i].HorizontalContentAlignment = HorizontalAlignment.Center;
                btns[i].VerticalAlignment = VerticalAlignment.Center;
                btns[i].FontSize = 32;
                btns[i].Content = Constants.buttonMenuLabels[i];
            }

            btns[0].Click += (object sender, RoutedEventArgs e) =>
            {
                this.PagesUse.Content = new QuestionTMP(QuestionTMP.Phase.SignUp);
            };

            btns[1].Click += (object sender, RoutedEventArgs e) =>
            {
                this.PagesUse.Content = new QuestionTMP(QuestionTMP.Phase.LogIn);
            };

            for (int i = 0; i < btns.Length; i++)
                this.ScrollMenu.Children.Add(btns[i]);

            usernameObject = "";

            HomePage hp = new HomePage();
            hp.AnimationWork = true;
            this.PagesUse.Content = hp;
        }

        public void sendData(requestStatusCode code, string data)
        {
            Response rsp = null;
            GetRoomResponse grrsp = null;
            GetStatisticsTopUsrResponse gstur = null;
            GetUserStatisticsResponse ugsrsp = null;
            byte status = 0;
            bool errMsgB = false;
            string errMsg = "";
            string possblUsrnm = "";

            // Just checking if any username is ready to use
            if (this.PagesUse.Content is QuestionTMP)
            {
                possblUsrnm = (this.PagesUse.Content as QuestionTMP).usernameTxt;
            }

            Task.Factory.StartNew(() =>
            {

                Action method = () =>
                {
                    byte[] ans = null;

                    try
                    {
                        try
                        {
                            cmnct.sendData(data, (byte)code);
                        }
                        catch (Exception e)
                        {
                            errMsgB = true;
                            errMsg = e.Message;
                            return;
                        }

                        ans = cmnct.receiveData();

                        if (ans == null)
                        {
                            errMsgB = true;
                            errMsg = "Invalid Connection! Socket connection was closed by the server!";
                            return;
                        }

                        status = ans[0];
                        string recvJson = Encoding.Default.GetString(ans).Substring(4);

                        switch (status)
                        {
                            case (byte)responseStatusCode.RSSC_GET_ROOMS:
                                grrsp = JsonConvert.DeserializeObject<GetRoomResponse>(recvJson);
                                rsp = new Response();
                                rsp.status = grrsp.status;
                                break;
                            case (byte)responseStatusCode.RSSC_GET_STATISTICS_TOP_USRS:
                                gstur = JsonConvert.DeserializeObject<GetStatisticsTopUsrResponse>(recvJson);
                                rsp = new Response();
                                rsp.status = gstur.status;
                                break;
                            case (byte)responseStatusCode.RSSC_GET_STATISTICS_USR:
                                ugsrsp = JsonConvert.DeserializeObject<GetUserStatisticsResponse>(recvJson);
                                rsp = new Response();
                                rsp.status = ugsrsp.status;
                                break;
                            default:
                                rsp = JsonConvert.DeserializeObject<Response>(recvJson);
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        rsp = new Response();
                        rsp.status = 0;
                        rsp.message = e.Message;
                    }
                };

                method();
            }).ContinueWith((Task tsk) =>
            {
                if (errMsgB)
                {
                    this.PagesUse.Content = null;
                    this.ScrollMenu.Children.Clear();
                    MenuInitializer();
                    MessageBoxResult result = MessageBox.Show(errMsg, "Error", MessageBoxButton.OK);
                    return;
                }

                if (rsp != null && rsp.status != Constants.Success)
                {
                    if (this.PagesUse.Content != null && this.PagesUse.Content is Page)
                    {
                        if ((this.PagesUse.Content as Page).Content is StackPanel)
                        {
                            object errTxt = ((this.PagesUse.Content as Page).Content as StackPanel).FindName("ErrorMsg");
                            if (errTxt is Label)
                            {
                                if (rsp.status == Constants.Fail)
                                {
                                    if (status == (byte)responseStatusCode.RSSC_ERROR)
                                        (errTxt as Label).Content = rsp.message;
                                    else
                                    {
                                        string dicData = "";
                                        if (Constants.errorTexts.TryGetValue((int)status, out dicData))
                                        { (errTxt as Label).Content = Constants.errorTexts[(int)status]; }
                                        else
                                        { (errTxt as Label).Content = Constants.InvalidCodeStr; }
                                        errMsgAnimationWork(ref errTxt);
                                    }
                                }
                                else
                                {
                                    (errTxt as Label).Content = "Invalid status code return from the server!";
                                    (errTxt as Label).Opacity = 0;
                                    (errTxt as Label).Opacity = 1;
                                    throw new Exception("Invalid status code return from the server!");
                                }
                            }
                        }
                    }
                    else if(rsp == null)
                    {
                        throw new Exception("Invalid data was given and parsed!");
                    }

                    if (rsp != null && rsp.status != Constants.Fail)
                    {
                        throw new Exception("Invalid status code return from the server! - No error text!");
                    }
                }
                else
                {

                    if (status == (byte)responseStatusCode.RSSC_LOGIN || status == (byte)responseStatusCode.RSSC_SIGNUP)
                    {
                        if(this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                        { (this.PagesUse.Content as HomePage).AnimationWork = false; }
                        usernameObject = possblUsrnm;
                        this.ScrollMenu.Children.Clear();
                        this.PagesUse.Content = null;

                        HomePage hp = new HomePage();
                        hp.AnimationWork = true;
                        this.PagesUse.Content = hp;
                        addButtons();
                    }
                    else if (status == (byte)responseStatusCode.RSSC_GET_ROOMS)
                    {
                        if (this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                        { (this.PagesUse.Content as HomePage).AnimationWork = false; }

                        try
                        {
                            createRoomList(ref grrsp);
                        }
                        catch (Exception e)
                        {
                            if (this.PagesUse.Content is Page)
                            {
                                if ((this.PagesUse.Content as Page).Content is StackPanel)
                                {
                                    object errTxt = ((this.PagesUse.Content as Page).Content as StackPanel).FindName("ErrorMsg");
                                    if (errTxt is Label)
                                    {
                                        (errTxt as Label).Content = e.Message;
                                        (errTxt as Label).Opacity = 0;
                                        (errTxt as Label).Opacity = 1;
                                    }
                                }
                            }
                        }
                    }
                    else if (status == (byte)responseStatusCode.RSSC_CREATE_ROOM || status == (byte)responseStatusCode.RSSC_JOIN_ROOM)
                    {
                        if (this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                        { (this.PagesUse.Content as HomePage).AnimationWork = false; }
                    }
                    else if (status == (byte)responseStatusCode.RSSC_GET_STATISTICS_TOP_USRS)
                    {
                        if (this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                        { (this.PagesUse.Content as HomePage).AnimationWork = false; }

                        try
                        {
                            createHighScore(gstur.data);
                        }
                        catch (Exception e)
                        {
                            if ((this.PagesUse.Content as Page).Content is StackPanel)
                            {
                                object errTxt = ((this.PagesUse.Content as Page).Content as StackPanel).FindName("ErrorMsg");
                                if (errTxt is Label)
                                {
                                    (errTxt as Label).Content = e.Message;
                                    (errTxt as Label).Opacity = 0;
                                    (errTxt as Label).Opacity = 1;
                                }
                            }

                        }
                    }
                    else if (status == (byte)responseStatusCode.RSSC_GET_STATISTICS_USR)
                    {
                        if (this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                        { (this.PagesUse.Content as HomePage).AnimationWork = false; }

                        try
                        {
                            createUserStats(ugsrsp.data);
                        }
                        catch (Exception e)
                        {
                            if ((this.PagesUse.Content as Page).Content is StackPanel)
                            {
                                object errTxt = ((this.PagesUse.Content as Page).Content as StackPanel).FindName("ErrorMsg");
                                if (errTxt is Label)
                                {
                                    (errTxt as Label).Content = e.Message;
                                    (errTxt as Label).Opacity = 0;
                                    (errTxt as Label).Opacity = 1;
                                }
                            }

                        }
                    }
                    else if(status == (byte)responseStatusCode.RSSC_LOGOUT)
                    {
                        if (this.PagesUse.Content != null && this.PagesUse.Content is HomePage)
                        { (this.PagesUse.Content as HomePage).AnimationWork = false; }

                        this.ScrollMenu.Children.Clear();
                        MenuInitializer();
                        this.PagesUse.Content = null;

                        HomePage hp = new HomePage();
                        hp.AnimationWork = true;
                        this.PagesUse.Content = hp;
                    }
                }

            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void addButtons()
        {
            Button[] btns = new Button[5];

            for (int i = 0; i < btns.Length; i++)
            {
                btns[i] = new Button();
                btns[i].Name = Constants.buttonMenuNames[i + 2];
                btns[i].Style = this.FindResource("MenuButton") as Style;
                btns[i].HorizontalAlignment = HorizontalAlignment.Stretch;
                btns[i].HorizontalContentAlignment = HorizontalAlignment.Center;
                btns[i].VerticalAlignment = VerticalAlignment.Center;
                btns[i].FontSize = 28;
                btns[i].Content = Constants.buttonMenuLabels[i + 2];
            }

            btns[0].Click += (object sender, RoutedEventArgs e) =>
            {
                this.PagesUse.Content = new QuestionTMP(QuestionTMP.Phase.CreateRoom);
            };
            btns[1].Click += (object sender, RoutedEventArgs e) =>
            {
                this.PagesUse.Content = new RoomsInfo();
            };

            btns[2].Click += (object sender, RoutedEventArgs e) =>
            {
                this.PagesUse.Content = new HighScoreInfo();
            };

            btns[3].Click += (object sender, RoutedEventArgs e) =>
            {
                this.PagesUse.Content = new UserStatsInfo();
            };

            btns[4].Click += (object sender, RoutedEventArgs e) =>
            {

                sendData(requestStatusCode.RQSC_LOGOUT, "{}");
            };

            for (int i = 0; i < btns.Length; i++)
                this.ScrollMenu.Children.Add(btns[i]);
        }

        private void createRoomList(ref GetRoomResponse grrsp)
        {
            RoomData[] rmdts = new RoomData[grrsp.data.Count()];

            for (int i = 0; i < rmdts.Length; i++)
            {
                RoomDataInfo rdi = grrsp.data[i];
                rmdts[i] = new RoomData(rdi);
                rmdts[i].Name = "RmNm" + rmdts[i].RoomName + rmdts[i].Id.ToString();
                rmdts[i].HorizontalAlignment = HorizontalAlignment.Center;
                rmdts[i].VerticalAlignment = VerticalAlignment.Center;
                uint id = rmdts[i].Id;

                rmdts[i].joinBTN.Click += (object sender, RoutedEventArgs e) =>
                {
                    JoinRoomRequest request = new JoinRoomRequest();
                    request.roomId = id;
                    this.sendData(requestStatusCode.RQSC_JOIN_ROOM, JsonConvert.SerializeObject(request));
                };
            }

            Action throwing = () =>
            {
                throw new Exception("Invalid rooms info page is chosen!");
            };

            if (this.PagesUse.Content is Page)
            {
                object d1 = (this.PagesUse.Content as Page).FindName("DataPage");
                if (d1 is StackPanel)
                {
                    object d2 = (d1 as StackPanel).FindName("QuestionMP");
                    if (d2 is Border)
                    {
                        object d3 = (d2 as Border).FindName("screenGrid");
                        if (d3 is Grid)
                        {
                            object d4 = (d3 as Grid).FindName("scrollV");
                            if (d4 is ScrollViewer)
                            {
                                object d5 = (d4 as ScrollViewer).FindName("scrollVGrid");
                                if (d5 is Grid)
                                {
                                    string[] stkpnlsNames = { "P1", "P2", "P3", "P4"};
                                    StackPanel[] stkpnls = new StackPanel[4];
                                    object stkpnlsVal = null;

                                    for (int i = 0; i < stkpnls.Length; i++)
                                    {
                                        stkpnlsVal = (d5 as Grid).FindName(stkpnlsNames[i]);
                                        if(stkpnlsVal is StackPanel)
                                        {
                                            stkpnls[i] = (stkpnlsVal as StackPanel);
                                            stkpnls[i].Children.Clear();
                                        }
                                    }

                                    for (int i = 0; i < rmdts.Length; i++)
                                    {
                                        stkpnls[i % stkpnls.Length].Children.Add(rmdts[i]);
                                    }
                                }
                                else throwing();
                            }
                            else throwing();
                        }
                        else throwing();
                    }
                    else throwing();
                }
                else throwing();
            }
            else throwing();
        }
        private void createHighScore(Dictionary<string, UserTopStats> data)
        {
            UserTopStats value = null;
            string[] username = new string[3];
            double[] ranking = new double[3];

            for (int i = 1; i <= 3; i++)
            {
                if (data.TryGetValue(i.ToString(), out value))
                {
                    username[i - 1] = value.username;
                    ranking[i - 1] = value.ranking;
                }
            }

            if (this.PagesUse.Content is HighScoreInfo)
            {
                (this.PagesUse.Content as HighScoreInfo).Username = username;
                (this.PagesUse.Content as HighScoreInfo).Ranking = ranking;
            }
            else
                throw new Exception("Statistics Block Wasn't Found");

        }

        private void createUserStats(UserGameStats data)
        {

            if (this.PagesUse.Content is UserStatsInfo)
            {
                (this.PagesUse.Content as UserStatsInfo).Ranking = data.ranking;
                (this.PagesUse.Content as UserStatsInfo).NumGames = data.num_games;
                (this.PagesUse.Content as UserStatsInfo).NumAnswers = data.num_answers;
                (this.PagesUse.Content as UserStatsInfo).CorrectAnswers = data.correct_answers;
                (this.PagesUse.Content as UserStatsInfo).WrongAnswers = data.wrong_answers;
                (this.PagesUse.Content as UserStatsInfo).AvgTimePerQuestion = data.avg_time_per_question;
                (this.PagesUse.Content as UserStatsInfo).ScorePoints = data.score_points;
            }
            else
                throw new Exception("Statistics Block Wasn't Found");
        }

        private void errMsgAnimationWork(ref object val)
        {
            if(val is Label)
            {
                Label val1 = val as Label;

                // Starting Values
                val1.Opacity = 1.0;
                val1.Visibility = Visibility.Visible;

                Dispatcher.Invoke(async () =>
                {
                    await Task.Delay(8799); // Just 1 milisecond before it appears
                    val1.Opacity = 0;
                    await Task.Delay(200); // Yeah that's just to finish the job nicely
                    val1.Visibility = Visibility.Collapsed;
                });
            }
        }
    }
}
