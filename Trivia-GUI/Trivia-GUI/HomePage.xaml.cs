﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_GUI
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    class rectPoints{
        public Point[] points = new Point[4];

        override
        public string ToString()
        {
            string s = "";

            for(int i = 0; i < points.Length; i++)
            {
                if(i > 0) { s += " "; }
                s += (i + 1) + ": (" + points[i].X + ", " + points[i].Y + ")";
            }

            return s;
        }

        public static bool intersectsWith(rectPoints r1, rectPoints r2)
        {
            foreach (var polygon in new[] { r1, r2 })
            {
                for (int i1 = 0; i1 < polygon.points.Length; i1++)
                {
                    int i2 = (i1 + 1) % polygon.points.Length;
                    var p1 = polygon.points[i1];
                    var p2 = polygon.points[i2];

                    var normal = new Point(p2.Y - p1.Y, p1.X - p2.X);

                    double? minA = null, maxA = null;
                    foreach (var p in r1.points)
                    {
                        var projected = normal.X * p.X + normal.Y * p.Y;
                        if (minA == null || projected < minA)
                            minA = projected;
                        if (maxA == null || projected > maxA)
                            maxA = projected;
                    }

                    double? minB = null, maxB = null;
                    foreach (var p in r2.points)
                    {
                        var projected = normal.X * p.X + normal.Y * p.Y;
                        if (minB == null || projected < minB)
                            minB = projected;
                        if (maxB == null || projected > maxB)
                            maxB = projected;
                    }

                    if (maxA < minB || maxB < minA)
                        return false;
                }
            }
            return true;
        }

    }

    public partial class HomePage : Page
    {
        private bool animationWork = false;
        private int animationDelay = -1;
        private string[] listStuff = { "Who are you?", "Who are we?", "Vat is dat?", "Answer de question...", "Stuff have gone crazy...",
                           "So... New here?", "A heart of a pure human!", "What are you doing?", "Don't worry mate...", "OK...?", "DATS TOUGH!",
                            "Welcome mate!", "Call me de human...", "Enjoying? Right???", "Come onnn...", "Bring it on."};
        private Dictionary<Border, rectPoints> lst = new Dictionary<Border, rectPoints>();

        public HomePage()
        {
            InitializeComponent();

            AnimationDelay = 1150;
        }
       
        ~HomePage()
        {
            this.animationWork = false;
        }

        public bool AnimationWork
        {
            set
            {
                animationWork = value;
                if (animationWork) { DoAnimation(); }
            }
            get { return animationWork; }
        }

        public int AnimationDelay
        {
            set
            {
                if(value > 10000)
                {
                    animationDelay = 10000;
                }
                else if (value < 100)
                {
                    animationDelay = 100;
                }
                else
                {
                    animationDelay = value;
                }
            }
            get { return animationDelay; }
        }

        private void DoAnimation()
        {
            Task.Factory.StartNew(async () =>
            {
                Random rnd = new Random();
                const double width = 240, height = 60;
                double maxloc = Math.Sqrt(width * width + height * height);
                int v = 0; // If no place was left so stop

                while (animationWork && v < 30)
                {
                    Border brd = new Border();
                    TextBlock txtblk = new TextBlock();
                    rectPoints rp = new rectPoints();
                    txtblk.Text = listStuff[rnd.Next(0, listStuff.Length - 1)];
                    txtblk.FontSize = 20;
                    txtblk.HorizontalAlignment = HorizontalAlignment.Center;
                    txtblk.VerticalAlignment = VerticalAlignment.Center;
                    brd.Background = new SolidColorBrush(Color.FromRgb((byte)rnd.Next(100, 255), (byte)rnd.Next(100, 255), (byte)rnd.Next(100, 255)));
                    brd.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                    brd.Width = width;
                    brd.Height = height;
                    brd.CornerRadius = new CornerRadius(5);
                    brd.Style = (this.FindResource("TxtFun") as Style);

                    bool work = true;
                    for (int i = 0; i < 30 && work; i++)
                    {
                        double angle = rnd.NextDouble() * (101) - 51;
                        double x = rnd.NextDouble() * (this.DrawingPage.Width - maxloc * 0.8);
                        double y = rnd.NextDouble() * (this.DrawingPage.Height - maxloc * 0.8);
                        
                        rp.points[0] = new Point(x, y); // bottom left
                        rp.points[1] = new Point(x + width, y); // bottom right
                        rp.points[2] = new Point(x + width, y + height); // top right 
                        rp.points[3] = new Point(x, y + height); // top left
                        object rpobj = (object)rp;

                        rotateRect(ref rpobj, angle);

                        //Console.WriteLine("Angle: " + angle + " Data: " + rp.ToString());

                        Canvas.SetLeft(brd, x);
                        Canvas.SetTop(brd, y);
                        brd.RenderTransform = new RotateTransform(angle, width / 2, height / 2);

                        bool innerwork = true;
                        foreach(var obj in lst)
                        {
                            if (rectPoints.intersectsWith(rp, obj.Value))
                            { innerwork = false; }
                        }

                        if(innerwork)
                        {
                            work = false;
                        }
                    }

                    if(!work)
                    {
                        v = 0;
                        lst.Add(brd, rp);
                        brd.Child = txtblk;
                        this.DrawingPage.Children.Add(brd);
                        destroy(ref brd);
                    }
                    else
                    {
                        v++;
                    }
                    
                    await Task.Delay(animationDelay);
                }
            }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void rotateRect(ref object obj, double angle)
        {
            if (obj is rectPoints)
            {
                rectPoints p = obj as rectPoints;
                Point[] coords = new Point[p.points.Length];
                double radAngle = angle * Math.PI / 180;
                double cx = (p.points[0].X + p.points[2].X) / 2, cy = (p.points[0].Y + p.points[2].Y) / 2; // The center of the rectangle

                for (int i = 0; i < coords.Length; i++)
                {
                    //(xcosθ−ysinθ ,xsinθ+ycosθ)
                    double tmpx = p.points[i].X - cx, tmpy = p.points[i].Y - cy;
                    coords[i].X = tmpx * Math.Cos(radAngle) - tmpy * Math.Sin(radAngle) + cx; // New Point
                    coords[i].Y = tmpx * Math.Sin(radAngle) + tmpy * Math.Cos(radAngle) + cy; // New Point
                }

                p.points = coords;
            }
        }

        private void destroy(ref Border obj)
        {
            Border obj1 = obj;
            Dispatcher.Invoke(async()=>
            {
                await Task.Delay(8799);
                obj1.Visibility = Visibility.Collapsed;
                await Task.Delay(200);
                this.DrawingPage.Children.Remove(obj1);
                lst.Remove(obj1);
            });
        }
    }
}
